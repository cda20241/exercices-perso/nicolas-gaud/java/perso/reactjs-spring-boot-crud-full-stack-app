import React, { Component } from 'react'
import UserService from '../services/UserService';
import UserTypeService from '../services/UserTypeService';
import UserTypeComponent from '../components/UserTypeComponent.tsx'

class CreateUserComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            user: {
                id: this.props.match.params.id,
                nickName: '',
                email: '',
                password: '',
                userType: {
                    id: '',
                    label: '',
                    codeType: ''
                }
            },
            userTypes: []
        };
        this.changeNickNameHandler = this.changeNickNameHandler.bind(this);
        this.changeEmailHandler = this.changeEmailHandler.bind(this);
//         this.changeUserTypeHandler = this.changeUserTypeHandler.bind(this);
        this.saveOrUpdateUser = this.saveOrUpdateUser.bind(this);
    }

    // step 3
    componentDidMount(){
        UserTypeService.getAllUserTypes().then((res) => {
            this.setState({ userTypes: res.data});
        });
        // step 4
        if(this.state.id === '_add'){
            return
        }else{
            UserService.getUserById(this.state.id).then( (res) =>{
                let user = res.data;
                this.setState({nickName: user.nickName,
                    email: user.email,
                    password : user.password
                    // TODO Add userType ?
                });
            });
        }        
    }
    saveOrUpdateUser = (e) => {
        e.preventDefault();
        let user = {nickName: this.state.nickName, email: this.state.email, password: this.state.password, userTypeId: this.state.user.userType.id};
        // TODO check if good ?
        console.log('user => ' + JSON.stringify(user));

        // step 5
        if(this.state.id === '_add'){
            UserService.createUser(user).then(res =>{
                this.props.history.push('/users');
            });
        }else{
            UserService.updateUser(user, this.state.id).then( res => {
                this.props.history.push('/users');
            });
        }
    }
    
    changeNickNameHandler= (event) => {
        this.setState({nickName: event.target.value});
    }

    changeEmailHandler= (event) => {
        this.setState({email: event.target.value});
    }

    changePasswordHandler= (event) => {
        this.setState({password: event.target.value});
    }

//     Spread Operator (...): When used in an object or array literal, it allows for the extraction of the properties or elements.
//     In the context of your code, when you see ...this.state.user, it creates a shallow copy of the user object, including all of its properties.
//     Similarly, ...this.state.user.userType creates a copy of the userType object.
//     Rest Operator (...): When used in function parameters, it is called the rest operator.
//     It allows you to represent an indefinite number of arguments as an array. However, in your code, you are using the spread operator.
//     In your case, the spread operator is used to ensure that you are not directly modifying the state but creating a new object with the necessary updates.
//     This is important in React because it helps maintain the immutability of the state, ensuring that you don't unintentionally mutate the state object.
//     Mutating the state directly can lead to unexpected behavior and issues in React applications.
//     changeUserTypeHandler = (event) => {
//         setSelected(event.target.value);
//         this.setState({
//             user: {
//                 ...this.state.user,
//                 userType: {
//                     ...this.state.user.userType,
//                     label: event.target.value
//                 }
//             }
//         });
//     }

    cancel(){
        this.props.history.push('/users');
    }

    getTitle(){
        if(this.state.id === '_add'){
            return <h3 className="text-center">Add User</h3>
        }else{
            return <h3 className="text-center">Update User</h3>
        }
    }
    render() {
        return (
            <div>
                <br></br>
                   <div className = "container">
                        <div className = "row">
                            <div className = "card col-md-6 offset-md-3 offset-md-3">
                                {
                                    this.getTitle()
                                }
                                <div className = "card-body">
                                    <form>
                                        <div className = "form-group">
                                            <label> NickName: </label>
                                            <input placeholder="NickName" name="nickName" className="form-control"
                                                value={this.state.nickName} onChange={this.changeNickNameHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> Email: </label>
                                            <input placeholder="Email" name="email" className="form-control"
                                                value={this.state.email} onChange={this.changeEmailHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> Password: </label>
                                            <input placeholder="Password" name="password" className="form-control"
                                                value={this.state.password} onChange={this.changePasswordHandler}/>
                                        </div>
                                        <div>
                                            {
                                                new UserTypeComponent()
                                            }
{/*                                         <FormControl> */}
{/*                                             <InputLabel>Selection</InputLabel> */}
{/*                                             <Select */}
{/*                                                 value={selected} */}
{/*                                                 onChange={changeUserTypeHandler}> */}
{/*                                                     {newUsersList.map(({ label, value }) => ( */}
{/*                                                       <MenuItem id={value} value={value}> */}
{/*                                                         {label} */}
{/*                                                       </MenuItem> */}
{/*                                                     ))} */}
{/*                                           </Select> */}
{/*                                         </FormControl> */}
{/*                                         <div className = "form-group"> */}
{/*                                             <label> User Type: </label> */}
{/*                                             <input placeholder="User Type" name="userType" className="form-control" */}
{/*                                                 value={this.state.user.userType.label} onChange={this.changeUserTypeHandler}/> */}
                                        </div>
                                        <button className="btn btn-success" onClick={this.saveOrUpdateUser}>Save</button>
                                        <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft: "10px"}}>Cancel</button>
                                    </form>
                                </div>
                            </div>
                        </div>

                   </div>
            </div>
        )
    }
}

export default CreateUserComponent
