import React, { useEffect, useState } from 'react';
import { UserType } from './UserType';
import UserTypeService from '../services/UserTypeService';

function UserTypeComponent(){
//     useState: This is a React hook used to declare state variables in functional components
//     It returns an array with two elements
//     The first element (userTypes in this case) is the current state value, and the second element (setUserTypes) is a function that allows you to update the state.
//
//     UserType[] | null: This part specifies the type of the state variable
//     It indicates that userTypes can either be an array of UserType objects (UserType[]) or null
//     The | symbol is a TypeScript syntax for a union type, allowing multiple types for a variable.
//
//     (null): This is the initial value assigned to the userTypes state variable
//     In this case, it's set to null.
//
//     So, in summary, this line of code is declaring a state variable named userTypes that can hold an array of UserType objects or be null, and its initial value is set to null
//     This is useful when dealing with asynchronous operations where the data might not be available immediately.
    const [userTypes, setUserTypes] = useState<UserType[] | null>(null);
    const [selected, setSelected] = useState<UserType[]>([]);

    useEffect(() => {
//         const url =
//             'http://localhost:8082/api/userTypes/all';
//         axios.get(url).then((response) => {
//             setUserTypes(response.data);
//         });
//            setUserTypes(UserTypeService.getUsers());
        const fetchData = async () => {
            const users = await UserTypeService.getAllUserTypes();
            setUserTypes(users);
        };
        fetchData();
    }, []);

    useEffect(() => {
        console.log('SELECTED:', selected);
    }, [selected]);

    return (
        <>
            <div>
                <select
                    onChange={(event) => {
                        const c = userTypes?.find((x) => x.id === event.target.value) as UserType;
                        setSelected([...selected, c]);
                    }}
                    defaultValue="default"
                >
                    <option value="default">Choose an option</option>
                    {userTypes
                        ? userTypes.map((userType) => {
                              return (
                                  <option key={userType.id} value={userType.id}>
                                      {userType.label}
                                  </option>
                              );
                          })
                        : null}
                </select>
            </div>
        </>
    );
}

export default UserTypeComponent;