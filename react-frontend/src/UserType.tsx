export type UserType = {
    id: number;
    label: string;
    codeType: string;
};