import axios from 'axios';

const USER_API_BASE_URL = "http://localhost:8082/api/users";

class UserService {

    getUsers(){
        return axios.get(USER_API_BASE_URL + "/all");
    }

    createUser(user){
        return axios.post(USER_API_BASE_URL, user);
    }

    getUserById(userId){
        return axios.get(USER_API_BASE_URL + '?id=' + userId);
    }

    getUserByEmail(email){
        return axios.get(USER_API_BASE_URL + '/' + email);
    }

    updateUser(user, userId){
        return axios.put(USER_API_BASE_URL + '/update/' + userId, user);
    }

    deleteUser(userId){
        return axios.delete(USER_API_BASE_URL + '/delete/' + userId);
    }
}

export default new UserService()