import axios from 'axios';

const USER_API_BASE_URL = "http://localhost:8082/api";

class UserTypeService {

    getAllUserTypes(){
        return axios.get(USER_API_BASE_URL + "/userTypes/all");
    }
}

export default new UserTypeService()