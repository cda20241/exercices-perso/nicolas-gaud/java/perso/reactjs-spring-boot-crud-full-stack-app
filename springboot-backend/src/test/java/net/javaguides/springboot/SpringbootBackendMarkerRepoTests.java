//package net.javaguides.springboot;
//
//import net.javaguides.springboot.modeldto.MarkerCreationDTO;
//import net.javaguides.springboot.modeldto.MarkerDTO;
//import net.javaguides.springboot.model.*;
//import net.javaguides.springboot.repository.MarkerRepository;
//import net.javaguides.springboot.service.MarkerService;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.data.domain.PageRequest;
//import org.springframework.data.domain.Pageable;
//import org.springframework.http.HttpStatus;
//
//import java.util.*;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//
//@SpringBootTest
//class SpringbootBackendMarkerRepoTests {
//
//	@Autowired
//	private MarkerService markerService;
//
//	@Autowired
//	private MarkerRepository markerRepository;
//
//	/**
//	 * This function tests the UpdateMarker() method from MarkerService by getting a Marker from DB,
//	 * changing its description and asserts that getting the same Marker description again equals the input description
//	 */
//	@Test
//	void testcountMarkersInIdsSet(){
//		Set<Integer> ids = new HashSet<>();
//		for (int i = 10; i < 20 ; i++){
//			ids.add(i);
//		}
//		Integer expected = ids.size();
//		Integer actual = markerRepository.countMarkers(ids);
//		assertEquals(expected, actual);
//	}
//
//	@Test
//	void testFindMarkersPerPageAndIds(){
//		int pageNumber = 2;
//		int pageSize = 32;
//		int expected = 25;
//		Set<Integer> ids = new HashSet<>();
//		for (int i = 1; i <= 150 ; i++){
//			ids.add(i);
//		}
//		Pageable pageable = PageRequest.of(pageNumber, pageSize); // Set pageNumber and pageSize accordingly
//		List<MarkerDTO> markerDTOS = markerRepository.findMarkersPerPage(pageable, ids);
//		System.out.println("Count : " + markerDTOS.size());
//		int actual = markerDTOS.size();
//		assertEquals(expected, actual);
//	}
//
//	@Test
//	void testFindMarkerMinIdFromIdSet(){
//		int id = 5;
//		int expected = 6;
//		int userTypeId = 4;
//        Set<Integer> ids = new HashSet<>();
//		for (int i = 1; i <= 50 ; i++){
//			ids.add(i);
//		}
//		assertEquals(expected, markerRepository.findMarkerMinIdFromIdSet(id, userTypeId, ids));
//	}
//
//	@Test
//	void testFindMarkerMaxIdFromIdSet(){
//		int id = 5;
//		int expected = 2;
//		int userTypeId = 4;
//		Set<Integer> ids = new HashSet<>();
//		for (int i = 1; i <= 50 ; i++){
//			ids.add(i);
//		}
//		assertEquals(expected, markerRepository.findMarkerMaxIdFromIdSet(id, userTypeId, ids));
//	}
//
//	@Test
//	void testDeleteMarker(){
//		MarkerCreationDTO markerCreationDTO = new MarkerCreationDTO("name", "description",
//				(float) 2.12345, (float) -2.3243, true,		"mapsLink",
//				new MarkerType(1, null, null) , new MarkerStatus(1, null, null),
//				new MarkerMotif(1, null, null), 4,
//				new Department("01", null, null), new Region(1, null, null),
//				new Country(150, "frName", "enNem", "frCAp", "enCap", 1));
//		User user = new User(1926, "Bill", "bill@gmail.com", "",  new UserType(1, null, null));
//		markerService.insertMarker(markerCreationDTO, user);
//		int id = markerService.getLastId();
//		System.out.println("Id : " + id);
//		Optional<MarkerDTO> optionalMarkerDTO = markerService.findMarkerById(id);
//		assertEquals(HttpStatus.OK, markerService.deleteMarkerAndImages(id).getStatusCode());
//	}
//
//	@Test
////	@Transactional
//	void testUpdateMarker(){
//		Marker marker = new Marker();
//		marker.setDescription("New description");
//		String expected = marker.getDescription();
//		String actual = "";
//		marker.setId(2000);
//		markerService.updateMarker(marker);
//		Optional<MarkerDTO> optionalResultMarkerDTO = markerService.findMarkerById(2000);
//		if (optionalResultMarkerDTO.isPresent()){
//			actual = optionalResultMarkerDTO.get().getDescription();
//			System.out.println(optionalResultMarkerDTO.get().getName());
//		}
//		assertEquals(expected, actual);
//	}
//}
