package net.javaguides.springboot;

import net.javaguides.springboot.exception.ResourceNotFoundException;
import net.javaguides.springboot.model.*;
import net.javaguides.springboot.modeldto.MarkerCreationDTO;
import net.javaguides.springboot.repository.MarkerRepository;
import net.javaguides.springboot.responsedto.MarkerResponseDTO;
import net.javaguides.springboot.service.MarkerService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Objects;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@SpringBootTest
class SpringbootBackendMarkerServiceTests {

	// Mocking MarkerRepository class
	@Mock
	MarkerRepository markerRepository;

	// Injecting mocks (ie : markerRepository) into the MarkerService
	@InjectMocks
	MarkerService markerService;

	/**
	 * This function tests the UpdateMarker() method from MarkerService by getting a Marker from DB,
	 * changing its description and asserts that getting the same Marker description again equals the input description
	 */
	@Test
//	@Transactional
	void testUpdateMarker(){
		MarkerCreationDTO markerCreationDTO = new MarkerCreationDTO("name", "description",
				(float) 2.12345, (float) -2.3243, true,		"mapsLink",
				new MarkerType(1, null, null) , new MarkerStatus(1, null, null),
				new MarkerMotif(1, null, null), 4,
				new Department("01", null, null), new Region(1, null, null),
				new Country(150, "frName", "enNem", "frCAp", "enCap", 1));
		Marker marker = MarkerCreationDTO.toMarker(markerCreationDTO);
		marker.setId(1901);
		marker.setDescription("NEW description");
		// mocks the return of "markerRepository.findById(1901)" by "marker"
		when(markerRepository.findById(1901)).thenReturn(Optional.of(marker));
//        assertEquals(HttpStatus.OK, markerService.updateMarker(marker).getStatusCode());
	}

	/**
	 * This function tests the UpdateMarker() method from MarkerService by getting a Marker from DB,
	 * changing its description and asserts that trying to update a non-existing Marker from DB throws a ResourceNotFoundException
	 */
	@Test
	void testUpdateMarkerThrowsException(){
		MarkerCreationDTO markerCreationDTO = new MarkerCreationDTO("name", "description",
				(float) 2.12345, (float) -2.3243, true,		"mapsLink",
				new MarkerType(1, null, null) , new MarkerStatus(1, null, null),
				new MarkerMotif(1, null, null), 4,
				new Department("01", null, null), new Region(1, null, null),
				new Country(150, "frName", "enNem", "frCAp", "enCap", 1));
		Marker marker = MarkerCreationDTO.toMarker(markerCreationDTO);
		marker.setId(2000);
//        assertThrows(ResourceNotFoundException.class, () -> markerService.updateMarker(marker));
	}

	/**
	 * This function tests the DeleteMarker() method from MarkerService by inserting a new Marker in DB,
	 * retrieving the last ID inserted (ie : max(id)) and asserting calling the DeleteMarker() method returns a
	 * 'HttpStatus.OK' code (200 : OK)
	 */
	@Test
	void testDeleteMarker(){
		MarkerCreationDTO markerCreationDTO = new MarkerCreationDTO("name", "description",
				(float) 2.12345, (float) -2.3243, true,		"mapsLink",
				new MarkerType(1, null, null) , new MarkerStatus(1, null, null),
				new MarkerMotif(1, null, null), 4,
				new Department("01", null, null), new Region(1, null, null),
				new Country(150, "frName", "enNem", "frCAp", "enCap", 1));
		Marker marker = MarkerCreationDTO.toMarker(markerCreationDTO);
		marker.setId(1926);
		User user = new User(1926, "Bill", "bill@gmail.com", "", new UserType(1, null, null));
		when(markerService.insertMarker(markerCreationDTO, user)).thenReturn(marker.getId());
		markerService.insertMarker(markerCreationDTO, user);
		when(markerRepository.findById(marker.getId())).thenReturn(Optional.of(marker));
		assertEquals(HttpStatus.OK, markerService.deleteMarkerAndImages(marker.getId()).getStatusCode());
	}

	/**
	 * This function tests the DeleteMarker() method from MarkerService by getting a Marker from DB,
	 * changing its description and asserts that trying to delete a non-existing Marker from DB throws a ResourceNotFoundException
	 */
	@Test
	void testDeleteMarkerThrowsException(){
		MarkerCreationDTO markerCreationDTO = new MarkerCreationDTO("name", "description",
				(float) 2.12345, (float) -2.3243, true,"mapsLink",
				new MarkerType(1, null, null) , new MarkerStatus(1, null, null),
				new MarkerMotif(1, null, null), 4,
				new Department("01", null, null), new Region(1, null, null),
				new Country(150, "frName", "enNem", "frCAp", "enCap", 1));
		Marker marker = MarkerCreationDTO.toMarker(markerCreationDTO);
		marker.setId(1926);
		User user = new User(1926, "Bill", "bill@gmail.com", "", new UserType(1, null, null));
		when(markerService.insertMarker(markerCreationDTO, user)).thenReturn(marker.getId());
		markerService.insertMarker(markerCreationDTO, user);
		when(markerRepository.findById(marker.getId())).thenReturn(Optional.of(marker));
		assertThrows(ResourceNotFoundException.class, () -> markerService.deleteMarkerAndImages(0));
	}

	/**
	 * This function tests the DeleteMarker() method from MarkerService by getting a Marker from DB,
	 * changing its description and asserts that trying to delete a non-existing Marker from DB throws a ResourceNotFoundException
	 */
	@Test
	void testDeleteMarkerResponseEntityKO(){
		MarkerCreationDTO markerCreationDTO = new MarkerCreationDTO("name", "description",
				(float) 2.12345, (float) -2.3243, true,"mapsLink",
				new MarkerType(1, null, null) , new MarkerStatus(1, null, null),
				new MarkerMotif(1, null, null), 4,
				new Department("01", null, null), new Region(1, null, null),
				new Country(150, "frName", "enNem", "frCAp", "enCap", 1));
		Marker marker = MarkerCreationDTO.toMarker(markerCreationDTO);
		marker.setId(1926);
		User user = new User(1926, "Bill", "bill@gmail.com", "", new UserType(1, null, null));
		when(markerService.insertMarker(markerCreationDTO, user)).thenReturn(marker.getId());
		markerService.insertMarker(markerCreationDTO, user);
		when(markerRepository.findById(marker.getId())).thenReturn(Optional.of(marker));
		ResponseEntity<MarkerResponseDTO> markerResponse = markerService.deleteMarkerAndImages(marker.getId());
		assertEquals(Boolean.FALSE, Objects.requireNonNull(markerResponse.getBody()).getSuccess());
	}
}
