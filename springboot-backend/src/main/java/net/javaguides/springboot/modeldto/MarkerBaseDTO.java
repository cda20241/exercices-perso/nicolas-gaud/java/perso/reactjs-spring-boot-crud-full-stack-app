package net.javaguides.springboot.modeldto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MarkerBaseDTO {

    private String name;
    private String description;
    private float latitude;
    private float longitude;
    private boolean isUrbex;
    private String mapsLink;

    public MarkerBaseDTO() {
        super();
    }

    public MarkerBaseDTO(String name, String description, float latitude, float longitude,
                         boolean isUrbex, String mapsLink) {
        this.name = name;
        this.description = description;
        this.latitude = latitude;
        this.longitude = longitude;
        this.isUrbex = isUrbex;
        this.mapsLink = mapsLink;
    }

    @JsonProperty("isUrbex")
    public boolean isUrbex() {
        return isUrbex;
    }
}

