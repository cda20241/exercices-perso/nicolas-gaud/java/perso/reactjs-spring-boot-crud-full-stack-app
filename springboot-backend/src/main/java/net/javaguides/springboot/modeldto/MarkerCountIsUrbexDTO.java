package net.javaguides.springboot.modeldto;

/**
 * DTO Class used to gather how many Markers are 'Urbex' and how many are not
 */
public class MarkerCountIsUrbexDTO {
    private boolean isUrbex;
    private long count;

    public MarkerCountIsUrbexDTO() {
        super();
    }

    public MarkerCountIsUrbexDTO(boolean isUrbex, long count) {
        super();
        this.isUrbex = isUrbex;
        this.count = count;
    }

    public boolean isUrbex() {
        return isUrbex;
    }

    public void setUrbex(boolean urbex) {
        isUrbex = urbex;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }
}
