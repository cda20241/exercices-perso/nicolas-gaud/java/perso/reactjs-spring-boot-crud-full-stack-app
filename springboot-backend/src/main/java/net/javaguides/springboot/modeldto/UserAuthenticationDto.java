package net.javaguides.springboot.modeldto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserAuthenticationDto {

    private Integer id;
    private String nickName;
    private String email;
    private String password;
    private String userType;
    private int userTypeId;
    private String token;

}
