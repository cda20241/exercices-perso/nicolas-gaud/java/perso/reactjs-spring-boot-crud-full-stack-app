package net.javaguides.springboot.modeldto;

import com.fasterxml.jackson.annotation.JsonProperty;
import net.javaguides.springboot.model.Image;

import java.util.Date;

public class ImageDTO {

    private Integer id;
    private String name;
    private Date dateTaken;
    private boolean isFront;
    private Integer userId;
    private int imageTypeId;

    public ImageDTO() {
        super();
    }

    public ImageDTO(Integer id, String name, Date dateTaken, boolean isFront, Integer userId, int imageTypeId) {
        super();
        this.id = id;
        this.name = name;
        this.dateTaken = dateTaken;
        this.isFront = isFront;
        this.userId = userId;
        this.imageTypeId = imageTypeId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateTaken() {
        return dateTaken;
    }

    public void setDateTaken(Date dateTaken) {
        this.dateTaken = dateTaken;
    }

    @JsonProperty("isFront")
    public boolean isFront() {
        return isFront;
    }

    public void setFront(boolean front) {
        isFront = front;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public int getImageTypeId() {
        return imageTypeId;
    }

    public void setImageTypeId(int imageTypeId) {
        this.imageTypeId = imageTypeId;
    }

    /**
     * This function maps an image to an ImageDTO that contains only data needed for the Front End
     * @param image Image
     * @return ImageDTO
     */
    public static ImageDTO toDto(Image image){
        Integer id = image.getId();
        String name = image.getName();
        Date dateTaken = image.getDateTaken();
        boolean isFront = image.isFront();
        Integer userId = image.getUserId();
        int imageTypeId = image.getImageTypeId();
        return new ImageDTO(id, name, dateTaken, isFront, userId, imageTypeId);
    }
}
