package net.javaguides.springboot.modeldto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import net.javaguides.springboot.model.Image;
import net.javaguides.springboot.model.Marker;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class MarkerDTO extends MarkerBaseDTO{

    private Integer id;
    private Integer levelId;
    private Integer typeId;
    private String typeLabel;
    private String typeCode;
    private Integer statusId;
    private String statusLabel;
    private String statusCode;
    private Integer motifId;
    private String motifLabel;
    private String nickname;
    private String departmentName;
    private String departmentId;
    private String regionName;
    private Integer regionId;
    private String countryName;
    private Integer countryId;
    private String firstImage;
    private Date dateVisited;
    private List<ImageDTO> imageDTOs;

    public MarkerDTO(Integer id, Integer levelId, String name, String description, float latitude, float longitude, boolean isUrbex,
                     String mapsLink,Integer typeId, String typeLabel, String typeCode, Integer statusId, String statusLabel, String statusCode,
                     Integer motifId, String motifLabel, String nickname, String departmentName, String departmentId, String regionName,
                     Integer regionId, String countryName, Integer countryId, List<ImageDTO> imageDTOs) {
        super(name, description, latitude, longitude, isUrbex, mapsLink);
        this.id = id;
        this.levelId = levelId;
        this.typeId = typeId;
        this.typeLabel = typeLabel;
        this.typeCode = typeCode;
        this.statusId = statusId;
        this.statusLabel = statusLabel;
        this.statusCode = statusCode;
        this.motifId = motifId;
        this.motifLabel = motifLabel;
        this.nickname = nickname;
        this.departmentName = departmentName;
        this.departmentId = departmentId;
        this.regionName = regionName;
        this.regionId = regionId;
        this.countryName = countryName;
        this.countryId = countryId;
        this.imageDTOs = imageDTOs;
    }


    public void setFirstImage(String firstImage) {
        this.firstImage = firstImage;
    }

    public void setDateVisited(Date dateVisited) {
        this.dateVisited = dateVisited;
    }

    @JsonProperty("images")
    public List<ImageDTO> getImageDTOs() {
        return imageDTOs;
    }

    public void setImageDTOs(List<ImageDTO> imageDTOs) {
        this.imageDTOs = imageDTOs;
    }

    /**
     * This function maps a Marker coming from the database into a MarkerDTO used to send and receive data
     * from the Front End.
     * @param marker Marker
     * @return MarkerDTO
     */
    public static MarkerDTO toDto(Marker marker) {
        Integer id = marker.getId();
        Integer levelId = marker.getLevelId();
        String name = marker.getName();
        String description = marker.getDescription();
        float latitude = marker.getLatitude();
        float longitude = marker.getLongitude();
        boolean isUrbex = marker.isUrbex();
        String mapsLink = marker.getMapsLink();
        Integer typeId = marker.getMarkerType().getId();
        String typeLabel = marker.getMarkerType().getLabel();
        String typeCode = marker.getMarkerType().getCodeType();
        Integer statusId = marker.getMarkerStatus().getId();
        String statusLabel = marker.getMarkerStatus().getLabel();
        String statusCode = marker.getMarkerStatus().getCodeStatus();
        Integer motifId = marker.getMarkerMotif() != null ? marker.getMarkerMotif().getId() : null;
        String motifLabel = marker.getMarkerMotif() != null ? marker.getMarkerMotif().getLabel() : null;
        String nickName = !marker.getUserMarkers().isEmpty() ? marker.getUserMarkers().get(0).getMarkerUser().getNickname() : null;
        String departmentName = marker.getDepartment() != null ? marker.getDepartment().getName() : null;
        String departmentId = marker.getDepartment() != null ? marker.getDepartment().getId() : null;
        String regionName = marker.getRegion() != null ?  marker.getRegion().getName() : null;
        Integer regionId = marker.getRegion() != null ?  marker.getRegion().getId() : null;
        String countryName = marker.getCountry().getFrName();
        Integer countryId = marker.getCountry().getId();

        MarkerDTO markerDTO = new MarkerDTO(id, levelId, name, description, latitude, longitude, isUrbex, mapsLink, typeId, typeLabel,
                typeCode, statusId, statusLabel, statusCode, motifId, motifLabel, nickName, departmentName, departmentId, regionName,
                regionId, countryName, countryId, null);

        List<ImageDTO> imageDTOs = new ArrayList<>();
        for (Image image : marker.getImages()) {
            imageDTOs.add(ImageDTO.toDto(image));
            if (image.isFront())
            {
                markerDTO.setFirstImage(image.getName());
                markerDTO.setDateVisited(image.getDateTaken());
            }
        }
        markerDTO.setImageDTOs(imageDTOs);
        return markerDTO;
    }
}
