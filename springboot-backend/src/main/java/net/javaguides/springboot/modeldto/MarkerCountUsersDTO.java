package net.javaguides.springboot.modeldto;

/**
 * DTO Class used to gather how many Markers have been visited by a User
 */
public class MarkerCountUsersDTO {
    private String nickname;
    private long count;

    public MarkerCountUsersDTO() {
        super();
    }

    public MarkerCountUsersDTO(String nickname, long count) {
        super();
        this.nickname = nickname;
        this.count = count;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) { this.count = count; }
}
