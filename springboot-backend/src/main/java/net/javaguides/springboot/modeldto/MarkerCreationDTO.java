package net.javaguides.springboot.modeldto;

import net.javaguides.springboot.model.*;

public class MarkerCreationDTO extends MarkerBaseDTO{
    private MarkerType markerType;
    private MarkerStatus markerStatus;
    private MarkerMotif markerMotif;
    private int levelId;
    private Department department;
    private Region region;
    private Country country;

    public MarkerCreationDTO() {
        super();
    }

    public MarkerCreationDTO(String name, String description, float latitude, float longitude, boolean isUrbex,
                             String mapsLink, MarkerType markerType, MarkerStatus markerStatus, MarkerMotif markerMotif, int levelId,
                             Department department, Region region, Country country) {
        super(name, description, latitude, longitude,isUrbex, mapsLink);
        this.markerType = markerType;
        this.markerStatus = markerStatus;
        this.markerMotif = markerMotif;
        this.levelId = levelId;
        this.department = department;
        this.region = region;
        this.country = country;
    }

    public MarkerType getMarkerType() {
        return markerType;
    }

    public void setMarkerType(MarkerType markerType) {
        this.markerType = markerType;
    }

    public MarkerStatus getMarkerStatus() {
        return markerStatus;
    }

    public void setMarkerStatus(MarkerStatus markerStatus) {
        this.markerStatus = markerStatus;
    }

    public MarkerMotif getMarkerMotif() {
        return markerMotif;
    }

    public void setMarkerMotif(MarkerMotif markerMotif) {
        this.markerMotif = markerMotif;
    }

    public int getLevelId() {
        return levelId;
    }

    public void setLevelId(int levelId) {
        this.levelId = levelId;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    /**
     * This function maps a MarkerCreationDTO used to send and get data from Front End
     * to a Marker that is insertable in database
     * @param markerCreationDTO MarkerCreationDTO
     * @return Marker to be inserted in DB
     */
    public static Marker toMarker(MarkerCreationDTO markerCreationDTO) {
        return new Marker(markerCreationDTO.getName(),
                markerCreationDTO.getDescription(),
                markerCreationDTO.getLatitude(),
                markerCreationDTO.getLongitude(),
                markerCreationDTO.isUrbex(),
                markerCreationDTO.getMapsLink(),
                markerCreationDTO.getMarkerType(),
                markerCreationDTO.getMarkerStatus(),
                markerCreationDTO.getMarkerMotif(),
                markerCreationDTO.getLevelId(),
                markerCreationDTO.getDepartment(),
                markerCreationDTO.getRegion(),
                markerCreationDTO.getCountry()
        );
    }
}
