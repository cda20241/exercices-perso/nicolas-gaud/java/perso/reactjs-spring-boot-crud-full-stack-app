package net.javaguides.springboot.config;

import net.javaguides.springboot.auth.JwtAuthenticationFilter;
import net.javaguides.springboot.constant.AppConstant;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

    private final JwtAuthenticationFilter jwtAuthFilter;
    private final AuthenticationProvider authenticationProvider;

    public SecurityConfig(JwtAuthenticationFilter jwtAuthFilter, AuthenticationProvider authenticationProvider) {
        this.jwtAuthFilter = jwtAuthFilter;
        this.authenticationProvider = authenticationProvider;
    }

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                // Spring documentation: Our recommendation is to use CSRF protection for any request that could be
                // processed by a browser by normal users. If you are only creating a service that is used by
                // non-browser clients, you will likely want to disable CSRF protection.
                // More info on CSRF protection with Spring: https://www.baeldung.com/spring-security-csrf
                .csrf()
                .disable()
                // When we create a User or when a User logs in, we don't need a token,
                // so we have to add to a whitelist 'auth' controllers in order to be accessible for everyone
                .authorizeHttpRequests()
                // list of urls from controllers to add to the whitelist
                // (** -> means that each url starting with "/auth/"
                // ie : all endpoints of the AuthenticationController)
                .requestMatchers("/auth/**")
                .permitAll()
                .requestMatchers("/userTypes/**")
                .permitAll()
                .requestMatchers("/markerTypes/**")
                .permitAll()
                .requestMatchers("/markers/**")
                .permitAll()
                .requestMatchers("/markerStatuses/**")
                .permitAll()
                .requestMatchers("/departments/**")
                .permitAll()
                .requestMatchers("/regions/**")
                .permitAll()
                .requestMatchers("/countries/**")
                .permitAll()
                .requestMatchers("/marker/**")
                .permitAll()
                // list of urls from controllers that need  a token and to be logged as a "User"
                .requestMatchers("/users/**")
                .hasAuthority(AppConstant.ADMIN)
                // Every other request needs a token
                .anyRequest()
                .authenticated()
                .and()
                // configuration of Session Management
                .sessionManagement()
                // Defining how we want to create our session (here 'stateless' because we want to create a new session
                // for each new request
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                // Defining the authentication provider
                .authenticationProvider(authenticationProvider)
                // Adding our filter before the username and password filter
                .addFilterBefore(jwtAuthFilter, UsernamePasswordAuthenticationFilter.class);

        return http.build();
    }
}