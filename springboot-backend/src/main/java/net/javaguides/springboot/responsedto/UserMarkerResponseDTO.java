package net.javaguides.springboot.responsedto;

import net.javaguides.springboot.model.Marker;
import net.javaguides.springboot.model.User;
import net.javaguides.springboot.model.UserMarker;

public class UserMarkerResponseDTO extends ResponseDTO{

    private static final String USER_MARKER = " user_marker";
    public static final String SUCCESS_DELETE = ResponseDTO.SUCCESS_DELETE + USER_MARKER + EOS;
    public static final String ERROR_DELETE = ResponseDTO.ERROR_DELETE + USER_MARKER + EOS;
    public static final String SUCCESS_UPDATE = ResponseDTO.SUCCESS_UPDATE + USER_MARKER + EOS;
    public static final String SUCCESS_INSERT = ResponseDTO.SUCCESS_INSERT + USER_MARKER + EOS;
    public static final String INSERT = "Insert";
    public static final String DELETE = "Delete";

    private final Marker marker;
    private final User user;
    private final UserMarker userMarker;

    public UserMarkerResponseDTO(Marker marker, User user, UserMarker userMarker) {
        super();
        this.marker = marker;
        this.user = user;
        this.userMarker = userMarker;
    }

    public UserMarkerResponseDTO(Marker marker, User user, UserMarker userMarker, String message, String method, boolean success) {
        super();
        this.marker = marker;
        this.user = user;
        this.userMarker = userMarker;
        super.setMessage(this.toString(message, method));
        super.setSuccess(success);
    }

    public String toString(String message, String method){
        return switch (method) {
            case INSERT -> message + WITH_MARKER_NAME + marker.getName() + WITH_USER_NAME + user.getNickname();
            case DELETE -> message + WITH_ID + this.userMarker.getId();
            default -> "";
        };

//        String result = "";
//        switch (method){
//            case INSERT:
//                result = message + WITH_MARKER_NAME + marker.getName() + WITH_USER_NAME + user.getNickname();
//                break;
//            case DELETE:
//                result = message + WITH_ID + this.userMarker.getId();
//                break;
//        }
//        return result;
    }
}
