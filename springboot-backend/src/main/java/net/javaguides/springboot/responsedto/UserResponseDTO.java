package net.javaguides.springboot.responsedto;

import net.javaguides.springboot.model.User;

public class UserResponseDTO extends ResponseDTO{

    private static final String USER = " user";
    public static final String SUCCESS_DELETE = ResponseDTO.SUCCESS_DELETE + USER + EOS;
    public static final String ERROR_DELETE = ResponseDTO.ERROR_DELETE + USER + EOS;
    public static final String SUCCESS_UPDATE = ResponseDTO.SUCCESS_UPDATE + USER + EOS;

    private final User user;

    public UserResponseDTO(User user) {
        super();
        this.user = user;
    }

    public UserResponseDTO(User user, String message, boolean success) {
        super();
        this.user = user;
        super.setMessage(this.toString(message));
        super.setSuccess(success);
    }

    public String toString(String message){
        return message + user.getNickname() + WITH_ID + user.getId();
    }
}
