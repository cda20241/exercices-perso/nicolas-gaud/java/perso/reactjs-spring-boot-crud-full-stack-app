package net.javaguides.springboot.responsedto;

public abstract class ResponseDTO {

    public static final String WITH_ID = ", with ID : ";
    public static final String WITH_MARKER_NAME = ", with marker name : ";
    public static final String WITH_USER_NAME = ", with user nickname : ";
    public static final String SUCCESS_DELETE = "Deleted";
    public static final String ERROR_DELETE = "Error deleting";
    public static final String SUCCESS_UPDATE = "Updated";
    public static final String SUCCESS_INSERT = "Inserted";

    public static final String EOS = " : ";

    private String message;
    private Boolean success;

    protected ResponseDTO() {
        super();
    }

    protected ResponseDTO(String message, Boolean success) {
        super();
        this.message = message;
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
