package net.javaguides.springboot.responsedto;

import net.javaguides.springboot.model.Marker;

public class MarkerResponseDTO extends ResponseDTO{

    private static final String MARKER = " marker";
    public static final String SUCCESS_DELETE = ResponseDTO.SUCCESS_DELETE + MARKER + EOS;
    public static final String ERROR_DELETE = ResponseDTO.ERROR_DELETE + MARKER + EOS;
    public static final String SUCCESS_UPDATE = ResponseDTO.SUCCESS_UPDATE + MARKER + EOS;

    private final Marker marker;

    public MarkerResponseDTO(Marker marker) {
        super();
        this.marker = marker;
    }

    public MarkerResponseDTO(Marker marker, String message, boolean success) {
        super();
        this.marker = marker;
        super.setMessage(this.toString(message));
        super.setSuccess(success);
    }

    public String toString(String message){
        return message + marker.getName() + WITH_ID + marker.getId();
    }
}
