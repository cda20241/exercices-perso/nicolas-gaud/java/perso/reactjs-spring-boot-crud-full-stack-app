package net.javaguides.springboot.repository;

import net.javaguides.springboot.model.MarkerMotif;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MarkerMotifRepository extends JpaRepository<MarkerMotif, Integer> {

}
