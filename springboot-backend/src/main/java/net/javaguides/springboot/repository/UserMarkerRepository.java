package net.javaguides.springboot.repository;

import net.javaguides.springboot.model.Marker;
import net.javaguides.springboot.model.UserMarker;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMarkerRepository extends JpaRepository<UserMarker, Integer> {
    Integer countByUserMarker(Marker marker);

}
