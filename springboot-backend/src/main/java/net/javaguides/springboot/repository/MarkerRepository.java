package net.javaguides.springboot.repository;

import net.javaguides.springboot.modeldto.MarkerCountIsUrbexDTO;
import net.javaguides.springboot.modeldto.MarkerDTO;
import net.javaguides.springboot.model.Marker;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Repository
public interface MarkerRepository extends JpaRepository<Marker, Integer> {

    /**
     * This function retrieves all the markers in the table marker, associated with Foreign Keys
     * LEFT JOIN is invoked in order to get all markers even if there is no User in the association table
     * @return List<Marker>
     */
    @Query("SELECT DISTINCT m FROM Marker m " +
            "LEFT JOIN FETCH m.userMarkers um " +
            "LEFT JOIN FETCH um.markerUser u " +
            "LEFT JOIN FETCH um.userMarker " +
            "WHERE m.levelId <= :userTypeId")
    List<Marker> findAllMarkers(@Param("userTypeId") int userTypeId);

    /**
     * This function count the number of Markers in the table based on their Ids (unique)
     * @return Integer (count Markers)
     */
    @Query(value = "SELECT COUNT(m.id) FROM Marker m WHERE m.levelId <= :userTypeId")
    Integer countMarkers(@Param("userTypeId") Integer userTypeId);

    /**
     * This function return the count of Markers in the table existing within a Set of Ids
     * @param ids Set<Integer>
     * @return Integer (count Ids found in table)
     */
    @Query(value = "SELECT COUNT(m.id) FROM Marker m WHERE m.id IN :ids")
    Integer countMarkers(@Param("ids") Set<Integer> ids);

    /**
     * This function retrieves the Markers from the table starting at the offset and getting a number of records
     * This in order to send only a defined numbers of records starting at the offset defined by the last Marker Id
     * shown in the calling page
     * @param pageable Pageable (defining pageNumber : start, pageSize : number of records returned
     * @return List<MarkerDTO>
     */
//    @Query("SELECT DISTINCT m FROM Marker m ORDER BY m.id")
    @Query(value = "SELECT DISTINCT m FROM Marker m " +
            "LEFT JOIN FETCH m.userMarkers um " +
            "LEFT JOIN FETCH um.markerUser u " +
            "LEFT JOIN FETCH um.userMarker " +
            "WHERE m.levelId <= :userTypeId",
            countQuery = "SELECT COUNT(DISTINCT m) FROM Marker m WHERE m.levelId <= :userTypeId")
    Page<Marker> findMarkersPerPage(@Param("pageable") Pageable pageable, @Param("userTypeId") int userTypeId);

    /**
     * This function retrieves the Markers from the table starting at the offset and getting a number of records
     * This in order to send only a defined numbers of records starting at the offset defined by the last Marker Id
     * shown in the calling page
     * @param pageable Pageable (defining pageNumber : start, pageSize : number of records returned
     * @param ids Set<Integer> : a Set of Ids to search in
     * @return List<MarkerDTO>
     */
    @Query("SELECT DISTINCT m FROM Marker m WHERE m.id IN :ids ")
    List<MarkerDTO> findMarkersPerPage(@Param("pageable") Pageable pageable, @RequestBody Set<Integer> ids);


    /**
     * This function retrieves the Min Marker Id within a given Set of Integers and higher than a given ID
     * Also filtered by Marker levelId
     * @param id Integer
     * @param userTypeId Integer
     * @param ids Set<Integer>
     * @return The Min ID found in the DB depending on the above conditions
     */
    @Query("SELECT m.id FROM Marker m " +
            "WHERE m.id = (" +
            "    SELECT MIN(m2.id) FROM Marker m2 WHERE m2.id > :id) " +
            "AND m.levelId <= :userTypeId " +
            "AND m.id IN :ids")
    Integer findMarkerMinIdFromIdSet(@Param("id") int id, @Param("userTypeId") int userTypeId, @Param("ids") Set<Integer> ids);

    /**
     * This function retrieves the Max Marker Id within a given Set of Integers and lower than a given ID,
     * filtered by Marker levelID and userTypeId
     * @param id Integer
     * @param userTypeId Integer
     * @param ids Set<Integer>
     * @return The Max ID found in the DB depending on the above conditions
     */
    @Query("SELECT m.id FROM Marker m " +
            "WHERE m.id = (" +
            "    SELECT MAX(m2.id) FROM Marker m2 WHERE m2.id < :id) " +
            "AND m.levelId <= :userTypeId " +
            "AND m.id IN :ids")
    Integer findMarkerMaxIdFromIdSet(@Param("id") int id, @Param("userTypeId") int userTypeId, @Param("ids") Set<Integer> ids);

    /**
     * This functions retrieves a List of uniquely Marker names, this list is used to fill the autocomplete search
     * field in the Front End, filtered by Marker levelID and userTypeId
     * @param userTypeId Integer
     * @return ArrayList<String>
     */
    @Query("SELECT m.name FROM Marker m " +
            "WHERE m.levelId <= :userTypeId")
    ArrayList<String> findAllMarkerNames(@Param("userTypeId") int userTypeId);

    /**
     * This function retrieves how many Markers are Urbex and how many are not, filtered by Marker levelID and userTypeId
     * @See net.javaguides.springboot.dto.MarkerCountIsUrbexDTO
     * @param userTypeId Integer
     * @return List<MarkerCountIsUrbexDTO>
     */
    @Query("SELECT NEW net.javaguides.springboot.modeldto.MarkerCountIsUrbexDTO(m.isUrbex, COUNT(m.isUrbex)) " +
            "FROM Marker m WHERE m.levelId <= :userTypeId GROUP BY m.isUrbex")
    List<MarkerCountIsUrbexDTO> countIsUrbex(@Param("userTypeId") int userTypeId);

    /**
     * This function retrieves how many Markers are Urbex and how many are not, filtered by Marker levelID and userTypeId
     * Triggered when user chooses a filter
     * @See net.javaguides.springboot.dto.MarkerCountIsUrbexDTO
     * @param userTypeId Integer
     * @return List<MarkerCountIsUrbexDTO>
     */
    @Query(value = "SELECT f.is_urbex, count(m.id) FROM marker m " +
            "RIGHT OUTER JOIN fake_isurbex f ON f.is_urbex = m.is_urbex" +
            "AND m.level_id <= :userTypeId " +
            "AND id NOT IN :ids " +
            "OR m.id IS NULL " +
            "GROUP BY f.is_urbex", nativeQuery = true)
    List<Object[]> updateIsUrbex(@Param("ids") Set<Integer> ids, @Param("userTypeId") int userTypeId);

    /**
     * This function retrieves how many Markers have been visited by each User in DB, filtered by Marker levelID
     * and userTypeId
     * @See net.javaguides.springboot.service.countMarkersPerUser
     * @param userTypeId Integer
     * @return List<MarkerCountUsersDTO>
     */
    @Query(value = "SELECT COALESCE(u.nickname, 'Unknown') AS nickname, COUNT(m.id) AS count " +
            "FROM marker m " +
            "LEFT JOIN user_marker um ON m.id = um.marker_id " +
            "LEFT JOIN user u ON u.id = um.user_id " +
            "WHERE m.level_id <= :userTypeId " +
            "GROUP BY COALESCE(u.nickname, 'Unknown') " +
            "ORDER BY COALESCE(u.nickname, 'Unknown') DESC", nativeQuery = true)
    List<Object[]> countMarkersByUser(@Param("userTypeId") int userTypeId);

    /**
     * This function retrieves how many Markers have been visited by each User in DB, filtered by Marker levelID
     * and userTypeId
     * Triggered when user chooses a filter
     * @See net.javaguides.springboot.service.countMarkersPerUser
     * @param userTypeId Integer
     * @return List<MarkerCountUsersDTO>
     */
    @Query(value = "SELECT fu.nickname AS nickname, count(m.id) AS count FROM marker m " +
            "LEFT JOIN went w on m.id = w.marker_id " +
            "LEFT JOIN user u on u.id = w.user_id " +
            "RIGHT OUTER JOIN fake_user fu ON fu.nickname = IFNULL(u.nickname, '') " +
            "AND (m.level_id <= :userTypeId " +
            "AND m.id NOT IN :ids " +
            ") OR m.id IS NULL " +
            "GROUP BY fu.nickname", nativeQuery = true)
    List<Object[]> updateMarkersByUser(@Param("ids") Set<Integer> ids, @Param("userTypeId") int userTypeId);

    /**
     * This function retrieves the last Id inserted in the table Marker
     * @return Integer
     */
    @Query(value = "SELECT LAST_INSERT_ID() AS last_id FROM marker", nativeQuery = true)
    Integer getLastId();

    /**
     * This function retrieves the min Id in the table Marker within a Set of Ids
     * @return Integer (min Id found in table)
     */
    @Query(value = "SELECT coalesce(MIN(id), 0) FROM Marker m WHERE m.id in :ids", nativeQuery = true)
    Integer getMinId(@Param("ids") Set<Integer> ids);

    /**
     * This function retrieves the max Id in the table Marker within a Set of Ids
     * @return Integer (max Id found in table)
     */
    @Query(value = "SELECT coalesce(MAX(id), 0) FROM Marker m WHERE m.id in :ids", nativeQuery = true)
    Integer getMaxId(@Param("ids") Set<Integer> ids);
}
