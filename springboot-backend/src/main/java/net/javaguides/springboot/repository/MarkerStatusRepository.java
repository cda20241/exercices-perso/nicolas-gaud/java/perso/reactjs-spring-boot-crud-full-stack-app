package net.javaguides.springboot.repository;

import net.javaguides.springboot.model.MarkerStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MarkerStatusRepository extends JpaRepository<MarkerStatus, Integer> {

}
