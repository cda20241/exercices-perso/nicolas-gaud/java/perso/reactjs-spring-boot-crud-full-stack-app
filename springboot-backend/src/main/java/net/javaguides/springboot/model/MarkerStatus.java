package net.javaguides.springboot.model;

import jakarta.persistence.*;

@Entity
@Table(name = "marker_status")
public class MarkerStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String label;
    @Column
    private String codeStatus;

    @Transient
    private int count;

    public MarkerStatus() {
        super();
    }

    public MarkerStatus(int id, String label, String codeStatus) {
        super();
        this.id = id;
        this.label = label;
        this.codeStatus = codeStatus;
    }

    public MarkerStatus(int id, String label, String codeStatus, int count) {
        super();
        this.id = id;
        this.label = label;
        this.codeStatus = codeStatus;
        this.count = count;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getCodeStatus() {
        return codeStatus;
    }

    public void setCodeStatus(String codeStatus) {
        this.codeStatus = codeStatus;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
