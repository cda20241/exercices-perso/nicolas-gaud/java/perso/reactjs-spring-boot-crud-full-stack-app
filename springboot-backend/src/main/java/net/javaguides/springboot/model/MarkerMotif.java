package net.javaguides.springboot.model;

import jakarta.persistence.*;

@Entity
@Table(name = "motif")
public class MarkerMotif {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String label;
    @Column
    private String codeMotif;

    public MarkerMotif() {
        super();
    }

    public MarkerMotif(int id, String label, String codeMotif) {
        super();
        this.id = id;
        this.label = label;
        this.codeMotif = codeMotif;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getCodeMotif() {
        return codeMotif;
    }

    public void setCodeMotif(String codeMotif) {
        this.codeMotif = codeMotif;
    }
}
