package net.javaguides.springboot.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "country")
public class Country {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column
    private String frName;
    @Column
    private String enName;
    @Column
    private String frCapitalCity;
    @Column
    private String enCapitalCity;
    @Column
    private int continentId;

    public Country() {
        super();
    }

    public Country(int id, String frName, String enName, String frCapitalCity, String enCapitalCity, int continentId) {
        super();
        this.id = id;
        this.frName = frName;
        this.enName = enName;
        this.frCapitalCity = frCapitalCity;
        this.enCapitalCity = enCapitalCity;
        this.continentId = continentId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFrName(String frName) {
        this.frName = frName;
    }

    public void setEnName(String enName) {
        this.enName = enName;
    }

    public void setFrCapitalCity(String frCapitalCity) {
        this.frCapitalCity = frCapitalCity;
    }

    public void setEnCapitalCity(String enCapitalCity) {
        this.enCapitalCity = enCapitalCity;
    }

    public void setContinentId(int continentId) {
        this.continentId = continentId;
    }
}
