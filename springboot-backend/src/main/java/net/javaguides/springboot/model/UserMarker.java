package net.javaguides.springboot.model;

import jakarta.persistence.*;

@Entity
@Table(name = "user_marker")
public class UserMarker{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "marker_id")
    Marker userMarker;

    @ManyToOne
    @JoinColumn(name = "user_id")
    User markerUser;

    public UserMarker() {
        super();
    }

    public UserMarker(Marker userMarker, User markerUser) {
        super();
        this.userMarker = userMarker;
        this.markerUser = markerUser;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Marker getUserMarker() {
        return userMarker;
    }

    public void setUserMarker(Marker userMarker) {
        this.userMarker = userMarker;
    }

    public User getMarkerUser() {
        return markerUser;
    }

    public void setMarkerUser(User markerUser) {
        this.markerUser = markerUser;
    }
}
