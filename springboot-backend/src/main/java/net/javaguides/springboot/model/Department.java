package net.javaguides.springboot.model;

import jakarta.persistence.*;

@Entity
@Table(name = "department")
public class Department {

    @Id
    private String id;

    @Column
    private String name;
    @ManyToOne
    @JoinColumn(name = "region_id")
    private Region region;
    @Transient
    private int count;

    public Department() {
        super();
    }

    public Department(String id, String name, Region region) {
        super();
        this.id = id;
        this.name = name;
        this.region = region;
    }

    public Department(String id, String name, Region region, int count) {
        super();
        this.id = id;
        this.name = name;
        this.region = region;
        this.count = count;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
