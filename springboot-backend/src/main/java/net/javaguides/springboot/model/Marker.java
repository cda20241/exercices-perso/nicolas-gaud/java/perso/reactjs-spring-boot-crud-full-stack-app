package net.javaguides.springboot.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "marker")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Marker {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column
    private String name;
    @Column
    private String description;
    @Column
    private float latitude;
    @Column
    private float longitude;
    @Column(name = "is_urbex")
    private boolean isUrbex;
    @Column(name = "maps_link")
    private String mapsLink;
    @ManyToOne
    @JoinColumn(name = "marker_type_id")
    private MarkerType markerType;
    @ManyToOne
    @JoinColumn(name = "marker_status_id")
    private MarkerStatus markerStatus;
    @ManyToOne
    @JoinColumn(name = "motif_id")
    private MarkerMotif markerMotif;
    @Column(name = "level_id")
    private Integer levelId;
    @ManyToOne
    @JoinColumn(name = "department_id")
    private Department department;
    @ManyToOne
    @JoinColumn(name = "region_id")
    private Region region;
    @ManyToOne
    @JoinColumn(name = "country_id")
    private Country country;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "marker", cascade = CascadeType.ALL)
    private List<Image> images;
    @JsonIgnore
    @OneToMany(mappedBy = "userMarker", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<UserMarker> userMarkers;

    public Marker() {
        super();
        this.userMarkers = new ArrayList<>();
    }

    public Marker(String name, String description, float latitude, float longitude, boolean isUrbex, String mapsLink,
                  MarkerType markerType, MarkerStatus markerStatus, MarkerMotif markerMotif, int levelId,
                  Department department, Region region, Country country) {
        super();
        this.name = name;
        this.description = description;
        this.latitude = latitude;
        this.longitude = longitude;
        this.isUrbex = isUrbex;
        this.mapsLink = mapsLink;
        this.markerType = markerType;
        this.markerStatus = markerStatus;
        this.markerMotif = markerMotif;
        this.levelId = levelId;
        this.department = department;
        this.region = region;
        this.country = country;
        this.userMarkers = new ArrayList<>();
    }

    public Marker(Integer id, String name, String description, float latitude, float longitude, boolean isUrbex,
                  String mapsLink, MarkerType markerType, MarkerStatus markerStatus, MarkerMotif markerMotif,
                  Integer levelId, Department department, Region region, Country country) {
        super();
        this.id = id;
        this.name = name;
        this.description = description;
        this.latitude = latitude;
        this.longitude = longitude;
        this.isUrbex = isUrbex;
        this.mapsLink = mapsLink;
        this.markerType = markerType;
        this.markerStatus = markerStatus;
        this.markerMotif = markerMotif;
        this.levelId = levelId;
        this.department = department;
        this.region = region;
        this.country = country;
        this.userMarkers = new ArrayList<>();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    @JsonProperty("isUrbex")
    public boolean isUrbex() {
        return isUrbex;
    }

    public void setIsUrbex(boolean isUrbex) { this.isUrbex = isUrbex; }

    public String getMapsLink() {
        return mapsLink;
    }

    public void setMapsLink(String mapsLink) {
        this.mapsLink = mapsLink;
    }

    public MarkerType getMarkerType() {
        return markerType;
    }

    public void setMarkerType(MarkerType markerType) {
        this.markerType = markerType;
    }

    public MarkerStatus getMarkerStatus() {
        return markerStatus;
    }

    public void setMarkerStatus(MarkerStatus markerStatus) {
        this.markerStatus = markerStatus;
    }

    public MarkerMotif getMarkerMotif() {
        return markerMotif;
    }

    public void setMarkerMotif(MarkerMotif markerMotif) {
        this.markerMotif = markerMotif;
    }

    public Integer getLevelId() {
        return levelId;
    }

    public void setLevelId(Integer levelId) {
        this.levelId = levelId;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public List<UserMarker> getUserMarkers() {
        return userMarkers;
    }

    public void setUserMarkers(List<UserMarker> userMarkers) {
        this.userMarkers = userMarkers;
    }
}
