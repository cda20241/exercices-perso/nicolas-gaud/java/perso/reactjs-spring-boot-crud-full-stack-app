package net.javaguides.springboot.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.Builder;
import lombok.experimental.Accessors;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

//@Data
@Accessors(chain = true)
@Builder
@Entity
@Table(name = "user")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class User implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "nickname")
    private String nickname;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @ManyToOne
    @JoinColumn(name = "user_type_id")
    private UserType userType;

    @JsonIgnore
    @OneToMany(mappedBy = "markerUser")
    List<UserMarker> userMarkers;

    public User() {
    }

    public User(String email, String password, UserType userType) {
        this.email = email;
        this.password = password;
        this.userType = userType;
    }

    public User(String nickname, String email, String password, UserType userType) {
        this.nickname = nickname;
        this.email = email;
        this.password = password;
        this.userType = userType;
    }

    public User(int id, String nickname, String email, String password, UserType userType) {
        this.id = id;
        this.nickname = nickname;
        this.email = email;
        this.password = password;
        this.userType = userType;
    }

    public User(int id, String nickname, String email, String password, UserType userType, List<UserMarker> userMarkers) {
        this.id = id;
        this.nickname = nickname;
        this.email = email;
        this.password = password;
        this.userType = userType;
        this.userMarkers = userMarkers;
    }


    /**
     * This tunction returns a list of names of roles instead of a list of role object
     * @return List<String> roles
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority(userType.getLabel()));
    }

    /**
     * This function returns the user's email that we use here as a login
     * @return email String
     */
    @Override
    public String getUsername() {
        return email;
    }

    /*
     * Modification des méthodes isAccountNonExpired(), isAccountNonLocked(), isCredentialsNonExpired(), 
     * et isEnabled() de l'entité User.java afin qu'elles retournent true sinon nous ne pourrons pas nous connecter 
     * car nous n'utilisons pas ces propriétés dans cet exemple:
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public List<UserMarker> getUserMarkers() {
        return userMarkers;
    }

    public void setUserMarkers(List<UserMarker> userMarkers) {
        this.userMarkers = userMarkers;
    }
}
