package net.javaguides.springboot.model;

import jakarta.persistence.*;

@Entity
@Table(name = "region")
public class Region {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String name;
    @ManyToOne
    @JoinColumn(name = "country_id")
    private Country country;
    @Transient
    private int count;

    public Region() {
        super();
    }

    public Region(int id, String name, Country country) {
        super();
        this.id = id;
        this.name = name;
        this.country = country;
    }

    public Region(int id, String name, Country country, int count) {
        super();
        this.id = id;
        this.name = name;
        this.country = country;
        this.count = count;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
