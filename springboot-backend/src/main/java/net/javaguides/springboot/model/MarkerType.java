package net.javaguides.springboot.model;

import jakarta.persistence.*;

@Entity
@Table(name = "marker_type")
public class MarkerType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String label;
    @Column
    private String codeType;

    @Transient
    private int count;

    public MarkerType() {
        super();
    }

    public MarkerType(int id, String label, String codeType) {
        super();
        this.id = id;
        this.label = label;
        this.codeType = codeType;
    }

    public MarkerType(int id, String label, String codeType, int count) {
        super();
        this.id = id;
        this.label = label;
        this.codeType = codeType;
        this.count = count;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getCodeType() {
        return codeType;
    }

    public void setCodeType(String codeType) {
        this.codeType = codeType;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
