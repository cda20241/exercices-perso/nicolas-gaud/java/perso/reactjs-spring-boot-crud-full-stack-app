package net.javaguides.springboot.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;

import java.util.Date;

@Entity
@Table(name = "image")
//@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Image {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String name;
    @Column
    private Date dateTaken;
    @Column
    private boolean isFront;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "marker_id")
//    @JsonBackReference
    @JsonIgnore
    private Marker marker;
    @Column
    private Integer userId;
    @Column
    private int imageTypeId;

    public Image() {
        super();
    }

    public Image(int id, String name, Date dateTaken, boolean isFront, Marker marker, Integer userId, int imageTypeId) {
        super();
        this.id = id;
        this.name = name;
        this.dateTaken = dateTaken;
        this.isFront = isFront;
        this.marker = marker;
        this.userId = userId;
        this.imageTypeId = imageTypeId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateTaken() {
        return dateTaken;
    }

    public void setDateTaken(Date dateTaken) {
        this.dateTaken = dateTaken;
    }

    @JsonProperty("isFront")
    public boolean isFront() {
        return isFront;
    }

    public void setFront(boolean front) {
        this.isFront = front;
    }

    public Marker getMarkerId() {
        return marker;
    }

    public void setMarkerId(Marker marker) {
        this.marker = marker;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public int getImageTypeId() {
        return imageTypeId;
    }

    public void setImageTypeId(int imageTypeId) {
        this.imageTypeId = imageTypeId;
    }
}
