package net.javaguides.springboot.constant;

public interface AppConstant {

    static final Integer DEFAULT_INT = -1;
    static final String DEFAULT_INITIAL_PAGE = "0";
    static final String DEFAULT_MARKER_PER_PAGE = "8";
    static final String SORT_ASC = "asc";
    static final String SORT_DESC = "desc";
    static final String PROPERTY_ID = "id";
    static final String USER = "User";
    static final String SUPER_USER = "Super User";
    static final String VIP = "Vip";
    static final String ADMIN = "Admin";

//    static final String CROSS_ORIGIN = "http://localhost:3000";
    static final String CROSS_ORIGIN = "http://localhost:4200";
}
