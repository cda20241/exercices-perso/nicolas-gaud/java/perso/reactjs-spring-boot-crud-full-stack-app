package net.javaguides.springboot.service;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import io.jsonwebtoken.Claims;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Service
@RequiredArgsConstructor
public class JwtService {

    // Can also be declared in the 'application.properties' file
    private static final String SECRET_KEY = "3272357538782F413F4428472D4B6150645367566B5970337336763979244226";

    // Extraction of the username (login = email for us)
    public String extractUsername(String jwt) {
        return extractClaim(jwt, Claims::getSubject);
    }

    // Extraction of a targeted claim from JWT token
    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    // Token generation without extra claims (extraClaims)
    public String generateToken(UserDetails userDetails) {
        return generateToken(new HashMap<>(), userDetails);
    }

    // Token generation with all extra claims (extraClaims)
    public String generateToken(
            Map<String, Object> extraClaims,
            UserDetails userDetails
    ) {
        return Jwts
                .builder()
                .setClaims(extraClaims) // Adding extra claims
                .setSubject(userDetails.getUsername()) // Adding username in order to identify the token owner afterward
                .setIssuedAt(new Date(System.currentTimeMillis())) // Setting when the token has been generated in order to compute its ending time
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 24)) // Setting validity time of the token
                .signWith(getSignInKey(), SignatureAlgorithm.HS256) // Getting the sing in key and declaring the signature algorithm
                .compact(); // Generates and returns the token
    }

    // Extraction of all claims from JWT token
    private Claims extractAllClaims(String token) {
        return Jwts
                .parserBuilder()
                .setSigningKey(getSignInKey())
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    private Key getSignInKey() {
        // Declaring the signature algorithm
        byte[] keyBytes = Decoders.BASE64.decode(SECRET_KEY);
        return Keys.hmacShaKeyFor(keyBytes);
    }

    // Validating the token. Verify if the token's owner is the request sender
    public boolean isTokenValid(String token, UserDetails userDetails) {
        final String username = extractUsername(token);
        return (username.equals(userDetails.getUsername())) && !isTokenExpired(token);
    }

    // Verifying expiration date of the token
    private boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    // Extracting expiration date from the token
    private Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

}
