package net.javaguides.springboot.service;

import net.javaguides.springboot.exception.ResourceNotFoundException;
import net.javaguides.springboot.model.Marker;
import net.javaguides.springboot.model.User;
import net.javaguides.springboot.model.UserMarker;
import net.javaguides.springboot.repository.MarkerRepository;
import net.javaguides.springboot.repository.UserMarkerRepository;
import net.javaguides.springboot.responsedto.UserMarkerResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserMarkerService {

    @Autowired
    private final UserMarkerRepository userMarkerRepository;

    @Autowired
    private final MarkerRepository markerRepository;

    public UserMarkerService(UserMarkerRepository userMarkerRepository, MarkerRepository markerRepository) {
        this.userMarkerRepository = userMarkerRepository;
        this.markerRepository = markerRepository;
    }

    /**
     * Simply gets all UserMarkers
     * @return List<UserMarker>
     */
    public List<UserMarker> getAllUserMarker(){
        return userMarkerRepository.findAll();
    }

    /**
     * This function inserts markerId and userId into the association table UserMarker
     * @param marker Marker
     * @param user User
     * @return ResponseEntity<UserMarkerResponseDTO>
     */
    
    public ResponseEntity<UserMarkerResponseDTO> insertUserMarker(Marker marker, User user) {
        UserMarker userMarker = new UserMarker(marker, user);
        userMarkerRepository.save(userMarker);
        return ResponseEntity.ok(new UserMarkerResponseDTO(marker, user, userMarker,
                UserMarkerResponseDTO.SUCCESS_INSERT, UserMarkerResponseDTO.INSERT, Boolean.TRUE));
    }

    /**
     * This function deletes a UserMarker defined by its Id
     * @param id int
     * @return ResponseEntity<UserMarkerResponseDTO>
     */
    
    public ResponseEntity<UserMarkerResponseDTO> deleteUserMarker(int id){
        UserMarker userMarker = userMarkerRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundException.MESSAGE_USER_MARKER_NOT_FOUND,
                        ResourceNotFoundException.LOG_USER_MARKER_NOT_FOUND, id));

        userMarkerRepository.delete(userMarker);
        return ResponseEntity.ok(new UserMarkerResponseDTO(null, null, userMarker,
                UserMarkerResponseDTO.SUCCESS_DELETE, UserMarkerResponseDTO.DELETE, Boolean.TRUE));
    }

    /**
     * This function gets a UserMarker defined by its Id
     * @param id int
     * @return UserMarker
     */
    
    public UserMarker getUserMarkerById(int id){
        return userMarkerRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundException.MESSAGE_USER_MARKER_NOT_FOUND,
                        ResourceNotFoundException.LOG_USER_MARKER_NOT_FOUND, id));
    }

    /**
     * This function retrieves the count of User that went to a Marker
     * @param id int
     * @return ResponseEntity<UserMarkerResponseDTO>
     */
    
    public Integer getCountUserMarkersByMarkerId(int id){
        return userMarkerRepository.countByUserMarker(markerRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundException.MESSAGE_USER_MARKER_NOT_FOUND,
                        ResourceNotFoundException.LOG_USER_MARKER_NOT_FOUND, id)));
    }




}

