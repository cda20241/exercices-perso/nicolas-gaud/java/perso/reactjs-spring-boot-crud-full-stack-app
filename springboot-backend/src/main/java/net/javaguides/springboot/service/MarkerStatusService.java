package net.javaguides.springboot.service;

import net.javaguides.springboot.exception.ResourceNotFoundException;
import net.javaguides.springboot.model.MarkerMotif;
import net.javaguides.springboot.model.MarkerStatus;
import net.javaguides.springboot.model.MarkerType;
import net.javaguides.springboot.repository.MarkerMotifRepository;
import net.javaguides.springboot.repository.MarkerStatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.HashMap;
import java.util.Map;

@Service
public class MarkerStatusService {

    @Autowired
    private MarkerStatusRepository markerStatusRepository;

    public MarkerStatusService(MarkerStatusRepository markerStatusRepository) {
        this.markerStatusRepository = markerStatusRepository;
    }

    public MarkerStatus getMarkerStatusById(int id){
        return markerStatusRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundException.MESSAGE_MARKER_STATUS_NOT_FOUND,
                        ResourceNotFoundException.LOG_MARKER_STATUS_NOT_FOUND, id));
    }

    /**
     * This function updates a MarkerStatus in the DB, identified by its ID
     * @param id Integer
     * @param markerStatusDetails MarkerStatus
     * @return ResponseEntity<MarkerStatus>
     */
    public ResponseEntity<MarkerStatus> updateMarkerStatus(int id, MarkerStatus markerStatusDetails){
        MarkerStatus markerStatus = markerStatusRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundException.MESSAGE_MARKER_STATUS_NOT_FOUND,
                        ResourceNotFoundException.LOG_MARKER_STATUS_NOT_FOUND, id));

        markerStatus.setLabel(markerStatusDetails.getLabel());

        MarkerStatus updatedMarkerStatus = markerStatusRepository.save(markerStatus);
        return ResponseEntity.ok(updatedMarkerStatus);
    }

    /**
     * This function deletes a MarkerStatus identified by its ID
     * @param id Integer
     * @return ResponseEntity<Map<String, Boolean>> Message indicating the MarkerStatus has been deleted
     */
    public ResponseEntity<Map<String, Boolean>> deleteMarkerStatus(int id){
        MarkerStatus markerStatus = markerStatusRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundException.MESSAGE_MARKER_STATUS_NOT_FOUND, ResourceNotFoundException.LOG_MARKER_STATUS_NOT_FOUND, id));

        markerStatusRepository.delete(markerStatus);
        Map<String, Boolean> response = new HashMap<>();
        response.put("Deleted MarkerStatus : " + markerStatus.getLabel() + " with ID : " + markerStatus.getId(), Boolean.TRUE);
        return ResponseEntity.ok(response);
    }
}
