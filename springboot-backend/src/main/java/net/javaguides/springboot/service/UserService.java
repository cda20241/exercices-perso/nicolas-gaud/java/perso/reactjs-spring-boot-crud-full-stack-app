package net.javaguides.springboot.service;

import net.javaguides.springboot.constant.AppConstant;
import net.javaguides.springboot.exception.ResourceNotFoundException;
import net.javaguides.springboot.model.User;
import net.javaguides.springboot.repository.UserRepository;
import net.javaguides.springboot.responsedto.UserResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * This function gets all Users
     * @return List<User>
     */
    
    public List<User> getAllUsers(){
        return userRepository.findAll();
    }

    /**
     * This function retrieves a User defined by its ID
     * @param id int
     * @return User
     */
    
    public User getUserById(int id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundException.MESSAGE_USER_NOT_FOUND,
                        ResourceNotFoundException.LOG_USER_NOT_FOUND, id));
    }

    /**
     * This function retrieves a User by its email
     * @param email String
     * @return User
     */
    
    public User getUserByEmail(String email) {
        return userRepository.findUserByEmail(email)
                .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundException.MESSAGE_USER_NOT_FOUND,
                        ResourceNotFoundException.LOG_USER_NOT_FOUND, AppConstant.DEFAULT_INT));
    }

    /**
     * This function saves a User in the DB
     * @param user User
     * @return User
     */
    
    public User createUser(User user) {
        return userRepository.save(user);
    }

    /**
     * This function updates a User by its given ID
     * @param id Integer
     * @param userDetails User containing data to be updated
     * @return ResponseEntity<User>
     */
    
    public ResponseEntity<UserResponseDTO> updateUser(int id, User userDetails){
        User user = userRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundException.MESSAGE_USER_NOT_FOUND,
                        ResourceNotFoundException.LOG_USER_NOT_FOUND, id));

        user.setNickname(userDetails.getNickname());
        user.setEmail(userDetails.getEmail());
        user.setPassword(userDetails.getPassword());

        User updatedUser = userRepository.save(user);
        UserResponseDTO userResponseDTO = new UserResponseDTO(updatedUser, UserResponseDTO.SUCCESS_UPDATE, Boolean.TRUE);
        return ResponseEntity.ok(userResponseDTO);
    }

    /**
     * This function deletes a User from database by its given ID after having verified the User exists in DB
     * @param id Integer
     * @return ResponseEntity<Map<String, Boolean>>
     */
    
    public ResponseEntity<UserResponseDTO> deleteUser(@PathVariable int id){
        User user = userRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundException.MESSAGE_USER_NOT_FOUND,
                        ResourceNotFoundException.LOG_USER_NOT_FOUND, id));
        userRepository.delete(user);
        UserResponseDTO userResponseDTO;
        if (userRepository.findById(id).isEmpty()){
            userResponseDTO = new UserResponseDTO(user, UserResponseDTO.SUCCESS_DELETE, Boolean.TRUE);
        }
        else{
            userResponseDTO = new UserResponseDTO(user, UserResponseDTO.ERROR_DELETE, Boolean.TRUE);
        }
        return ResponseEntity.ok(userResponseDTO);
    }
}
