package net.javaguides.springboot.service;

import net.javaguides.springboot.constant.AppConstant;
import net.javaguides.springboot.exception.ResourceNotFoundException;
import net.javaguides.springboot.model.*;
import net.javaguides.springboot.modeldto.MarkerCountIsUrbexDTO;
import net.javaguides.springboot.modeldto.MarkerCountUsersDTO;
import net.javaguides.springboot.modeldto.MarkerCreationDTO;
import net.javaguides.springboot.modeldto.MarkerDTO;
import net.javaguides.springboot.repository.MarkerRepository;
import net.javaguides.springboot.responsedto.MarkerResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class MarkerService {
    @Autowired
    private final MarkerRepository markerRepository;
    private final MarkerTypeService markerTypeService;
    private final MarkerStatusService markerStatusService;
    private final MarkerMotifService markerMotifService;
    private final DepartmentService departmentService;
    private final RegionService regionService;
    private final CountryService countryService;

    //Constructor
    public MarkerService(MarkerRepository markerRepository, MarkerTypeService markerTypeService,
                         MarkerStatusService markerStatusService, MarkerMotifService markerMotifService,
                         DepartmentService departmentService, RegionService regionService, CountryService countryService)
    {
        this.markerRepository = markerRepository;
        this.markerTypeService = markerTypeService;
        this.markerStatusService = markerStatusService;
        this.markerMotifService = markerMotifService;
        this.departmentService = departmentService;
        this.regionService = regionService;
        this.countryService = countryService;
    }


    /**
     * This function retrieves all the markers, associated or not with a User
     * Returned Markers are based on the userTypeId and the Marker levelId
     * @param userTypeId int
     * @return List<MarkerDTO> List of custom Markers that is ready to be transmitted to the Front End
     */
    
    public List<MarkerDTO> findAllMarkers(int userTypeId) {
        List<Marker> markers = markerRepository.findAllMarkers(userTypeId);
        List<MarkerDTO> markerDTOS = new ArrayList<>();
        for (Marker marker : markers){
            MarkerDTO markerDTO = MarkerDTO.toDto(marker);
            markerDTOS.add(markerDTO);
        }
        return markerDTOS;
    }

    
    public int countMarkers(int userTypeId) {
        return markerRepository.countMarkers(userTypeId);
    }

    /**
     * This function gets the count of Marker present inside a given Set of Ids
     * @param ids Set<Integer>
     * @return int
     */
    
    public int countMarkers(Set<Integer> ids) {
        return markerRepository.countMarkers(ids);
    }

    /**
     * This function retrieves the Markers from the table starting at the offset and getting a number of records and then
     * convert them into MarkerDTOs
     * This in order to send only a defined numbers of records starting at the offset defined by the last Marker Id
     * shown in the calling page
     *
     * @param page int (1rst param to build Pageable)
     * @param size int (2nd param to build Pageable)
     * @param sort String (3rd param to build Pageable)
     * @return List<Marker>
     */
    
//    public Page<MarkerDTO> findMarkersPerPage(int userTypeId, int page, int size, String sort) {
//        Page<MarkerDTO> markerDTOPage;
//        Pageable pageable = PageRequest.of(page, size, Sort.by(sort.equalsIgnoreCase(AppConstant.SORT_DESC) ?
//                Sort.Direction.DESC : Sort.Direction.ASC, AppConstant.PROPERTY_ID));
//        List<Marker> markers = markerRepository.findMarkersPerPage(pageable, userTypeId);
//        List<MarkerDTO> markerDTOS = new ArrayList<>();
//        for (Marker marker : markers){
//            MarkerDTO markerDTO = MarkerDTO.toDto(marker);
//            markerDTOS.add(markerDTO);
//        }
//        markerDTOPage.add(markerDTOS);
//        return markerDTOPage;
//    }
    public Page<MarkerDTO> findMarkersPerPage(int userTypeId, int page, int size, String sort) {
        Pageable pageable = PageRequest.of(page, size, Sort.by(sort.equalsIgnoreCase(AppConstant.SORT_DESC) ?
                Sort.Direction.DESC : Sort.Direction.ASC, AppConstant.PROPERTY_ID));

        // Fetch data as Page<Marker>
        Page<Marker> markersPage = markerRepository.findMarkersPerPage(pageable, userTypeId);

        // Convert each Marker entity to MarkerDTO entity using Java streams
        List<MarkerDTO> markerDTOs = markersPage.getContent()
                .stream()
                .map(MarkerDTO::toDto) // Assuming you have a static method in MarkerDTO class to convert Marker to MarkerDTO
                .collect(Collectors.toList());

        // Create a new Page object using the converted MarkerDTO list
        return new PageImpl<>(markerDTOs, pageable, markersPage.getTotalElements());
    }


    /**
     * This function retrieves the Markers from the table starting at the offset and getting a number of records and then
     * convert them into MarkerDTOs
     * This in order to send only a defined numbers of records starting at the offset defined by the last Marker Id
     * shown in the calling page
     * @param page int (1rst param to build Pageable)
     * @param size int (2nd param to build Pageable)
     * @param sort String (3rd param to build Pageable)
     * @param ids Set<Integer> : a Set of Ids to search in
     * @return List<MarkerDTO>
     */
    
    public List<MarkerDTO> findMarkersPerPage(int page, int size, String sort, Set<Integer> ids) {
        Pageable pageable = PageRequest.of(page, size, Sort.by(sort.equalsIgnoreCase(AppConstant.SORT_DESC) ?
                Sort.Direction.DESC : Sort.Direction.ASC, AppConstant.PROPERTY_ID));
        return markerRepository.findMarkersPerPage(pageable, ids);
    }

    /**
     * This function retrieves a Marker by its Id and levelId within a Set of Ids, then convert it into a MarkerDTO
     * @See MarkerRepository.findMarkerMinIdFromIdSet and MarkerRepository.findMarkerMaxIdFromIdSet
     * @param id int
     * @param userTypeId int
     * @param dir String
     * @param ids Set<Integer>
     * @return Optional<MarkerDTO>
     */
    
    public Optional<MarkerDTO> findMarkerById(int id, int userTypeId, String dir, Set<Integer> ids) {
        int markerId = -1;
        if (dir.equalsIgnoreCase(AppConstant.SORT_ASC))
            markerId = markerRepository.findMarkerMinIdFromIdSet(id, userTypeId, ids);
        else if (dir.equalsIgnoreCase(AppConstant.SORT_DESC))
            markerId = markerRepository.findMarkerMaxIdFromIdSet(id, userTypeId, ids);
        if (markerRepository.findById(markerId).isPresent()) {
            MarkerDTO markerDTO = MarkerDTO.toDto(markerRepository.findById(markerId).get());
            return Optional.of(markerDTO);
        }
        else
            return Optional.empty();
    }

    /**
     * This function retrieves a Marker by its Id and then convert it into a MarkerDTO
     * @param id int
     * @return Optional<MarkerDTO>
     */
    
    public Optional<MarkerDTO> findMarkerById(int id) {
        Optional<Marker> marker = markerRepository.findById(id);
        if (marker.isPresent()){
            MarkerDTO markerDTO = MarkerDTO.toDto(marker.get());
            return Optional.of(markerDTO);
        }
        else
            return Optional.empty();
    }

    /**
     * This functions retrieves a List of uniquely Marker names, this list is used to fill the autocomplete search
     * field in the Front End, filtered by Marker levelID and userTypeId
     * @param userTypeId Integer
     * @return ArrayList<String>
     */
    
    public List<String> findAllMarkerNames(int userTypeId) {
        return markerRepository.findAllMarkerNames(userTypeId);
    }

    /**
     * This function retrieves how many Markers are Urbex and how many are not, filtered by Marker levelID and userTypeId
     * @See net.javaguides.springboot.dto.MarkerCountIsUrbexDTO
     * @param userTypeId Integer
     * @return List<MarkerCountIsUrbexDTO>
     */
    
    public List<MarkerCountIsUrbexDTO> countIsUrbex(int userTypeId) {
        return markerRepository.countIsUrbex(userTypeId);
    }

    /**
     * This function retrieves how many Markers are Urbex and how many are not, filtered by Marker levelID and userTypeId
     * within a Set of Integer
     *      * Triggered when user chooses filters and refreshes the counts
     * @See net.javaguides.springboot.dto.MarkerCountIsUrbexDTO
     * @param ids Set<Integer>
     * @param userTypeId int
     * @return List<MarkerCountIsUrbexDTO>
     */
    
    public List<MarkerCountIsUrbexDTO> updateIsUrbex(Set<Integer> ids, int userTypeId) {
        List<Object[]> resultList = markerRepository.updateIsUrbex(ids, userTypeId);
        List<MarkerCountIsUrbexDTO> markerCountIsUrbexDTOS = new ArrayList<>();

        for (Object[] row : resultList) {
            boolean isUrbex = (boolean) row[0];
            long count = (long) row[1];
            MarkerCountIsUrbexDTO markerCountIsUrbexDTO = new MarkerCountIsUrbexDTO(isUrbex, count);
            markerCountIsUrbexDTOS.add(markerCountIsUrbexDTO);
        }
        return markerCountIsUrbexDTOS;
    }

    /**
     * This function retrieves how many Markers have been visited by each User in DB, filtered by Marker levelID
     * and userTypeId
     * @param userTypeId int
     * @return List<MarkerCountUsersDTO>
     */
    public List<MarkerCountUsersDTO> countIsVisited(@Param("userTypeId") int userTypeId){
        List<Object[]> resultList = markerRepository.countMarkersByUser(userTypeId);
        List<MarkerCountUsersDTO> markerCountUsersDTOS = new ArrayList<>();

        for (Object[] row : resultList) {
            String nickname = (String) row[0];
            long count = (long) row[1];
            markerCountUsersDTOS.add(new MarkerCountUsersDTO(nickname, count));
        }
        return markerCountUsersDTOS;
    }

    /**
     * This function retrieves how many Markers have been visited by each User in DB, filtered by Marker levelID
     * and userTypeId
     *      * Triggered when user chooses filters and refreshes the counts
     * @param ids Set<Integer> ids
     * @param userTypeId int
     * @return List<MarkerCountUsersDTO>
     */
    
    public List<MarkerCountUsersDTO> updateIsVisited(Set<Integer> ids, int userTypeId) {
        List<Object[]> resultList = markerRepository.updateMarkersByUser(ids, userTypeId);
        List<MarkerCountUsersDTO> markerCountUsersDTOS = new ArrayList<>();

        for (Object[] row : resultList) {
            String nickname = (String) row[0];
            long count = (long) row[1];
            markerCountUsersDTOS.add(new MarkerCountUsersDTO(nickname, count));
        }
        return markerCountUsersDTOS;
    }

    /**
     * This function retrieves the min Id in the table Marker within a Set of Ids
     * @return Integer (min Id found in table)
     */
    
    public Integer getMinId(Set<Integer> ids) {
        return markerRepository.getMinId(ids);
    }

    /**
     * This function retrieves the max Id in the table Marker within a Set of Ids
     * @return Integer (max Id found in table)
     */
    
    public Integer getMaxId(Set<Integer>ids) {
        return markerRepository.getMaxId(ids);
    }

    /**
     * This function inserts a Marker in the DB
     * @param markerCreationDTO MarkerCreationDTO to be mapped
     * @param user User
     * @return Marker saved
     */
    
    public Integer insertMarker(MarkerCreationDTO markerCreationDTO, User user) {
        Marker marker = MarkerCreationDTO.toMarker(markerCreationDTO);
        marker.setLevelId(user.getUserType().getId());
        return markerRepository.save(marker).getId();
    }


    /**
     * This function updates a Marker in the DB, identified by its ID
     * @param markerDetails Marker with data to be updated
     * @return ResponseEntity<Marker> containing the updated Marker
     */
    public ResponseEntity<MarkerResponseDTO> updateMarker(MarkerDTO markerDetails) {
        Marker marker = markerRepository.findById(markerDetails.getId())
                .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundException.MESSAGE_MARKER_NOT_FOUND,
                        ResourceNotFoundException.LOG_MARKER_NOT_FOUND, markerDetails.getId()));

        //Setting attributes, according to @Annotations, some are objects, instead of IDs as foreign key
        marker.setName(markerDetails.getName());
        marker.setDescription(markerDetails.getDescription());
        marker.setLatitude(markerDetails.getLatitude());
        marker.setLongitude(markerDetails.getLongitude());
        marker.setIsUrbex(markerDetails.isUrbex());
        marker.setMapsLink(markerDetails.getMapsLink());
        marker.setMarkerType(this.markerTypeService.getMarkerTypeById(markerDetails.getTypeId()));
        marker.setMarkerStatus(this.markerStatusService.getMarkerStatusById(markerDetails.getStatusId()));
        marker.setMarkerMotif(markerDetails.getMotifId() != null ? this.markerMotifService.getMarkerMotifById(markerDetails.getMotifId()) : null);
        marker.setLevelId(markerDetails.getLevelId());
        marker.setDepartment(markerDetails.getDepartmentId() != null ? this.departmentService.getDepartmentById(markerDetails.getDepartmentId()) : null);
        marker.setRegion(markerDetails.getRegionId() != null ? this.regionService.getRegionById(markerDetails.getRegionId()) : null);
        marker.setCountry(this.countryService.getCountryById(markerDetails.getCountryId()));

        Marker updatedMarker = markerRepository.save(marker);
        MarkerResponseDTO markerResponseDTO = new MarkerResponseDTO(updatedMarker, MarkerResponseDTO.SUCCESS_UPDATE, Boolean.TRUE);
        return ResponseEntity.ok(markerResponseDTO);
    }

    /**
     * This function retrieves the last ID inserted in the table Marker
     * @return Integer
     */
    
    public Integer getLastId() {
        return markerRepository.getLastId();
    }

    /**
     * This function deletes a Marker identified by its ID
     * @param id Integer
     * @return ResponseEntity<Map<String, Boolean>> Message indicating the Marker has been deleted
     */
    
    public ResponseEntity<MarkerResponseDTO> deleteMarkerAndImages(int id){
        Marker marker = markerRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundException.MESSAGE_MARKER_NOT_FOUND,
                        ResourceNotFoundException.LOG_MARKER_NOT_FOUND, id));
        markerRepository.delete(marker);
        MarkerResponseDTO markerResponseDTO;
        if (markerRepository.findById(id).isEmpty()){
            markerResponseDTO = new MarkerResponseDTO(marker, MarkerResponseDTO.SUCCESS_DELETE, Boolean.TRUE);
        }
        else{
            markerResponseDTO = new MarkerResponseDTO(marker, MarkerResponseDTO.ERROR_DELETE, Boolean.TRUE);
        }
        return ResponseEntity.ok(markerResponseDTO);
    }
}
