package net.javaguides.springboot.service;

import net.javaguides.springboot.exception.ResourceNotFoundException;
import net.javaguides.springboot.model.Country;
import net.javaguides.springboot.model.Department;
import net.javaguides.springboot.model.Region;
import net.javaguides.springboot.repository.CountryRepository;
import net.javaguides.springboot.repository.RegionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.HashMap;
import java.util.Map;

@Service
public class RegionService {

    @Autowired
    private RegionRepository regionRepository;

    public RegionService(RegionRepository regionRepository) {
        this.regionRepository = regionRepository;
    }

    public Region getRegionById(int id){
        return regionRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundException.MESSAGE_REGION_NOT_FOUND,
                        ResourceNotFoundException.LOG_REGION_NOT_FOUND, id));
    }

    /**
     * This function updates a Region in the DB, identified by its ID
     * @param id Integer
     * @param regionDetails Region
     * @return ResponseEntity<Region>
     */
    public ResponseEntity<Region> updateRegion(int id, Region regionDetails){
        Region region = regionRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundException.MESSAGE_REGION_NOT_FOUND, ResourceNotFoundException.LOG_REGION_NOT_FOUND, id));

        region.setName(regionDetails.getName());
        region.setCountry(regionDetails.getCountry());

        Region updatedRegion = regionRepository.save(region);
        return ResponseEntity.ok(updatedRegion);
    }

    /**
     * This function deletes a Region identified by its ID
     * @param id Integer
     * @return ResponseEntity<Map<String, Boolean>> Message indicating the Region has been deleted
     */
    public ResponseEntity<Map<String, Boolean>> deleteRegion(int id){
        Region region = regionRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundException.MESSAGE_REGION_NOT_FOUND, ResourceNotFoundException.LOG_REGION_NOT_FOUND, id));

        regionRepository.delete(region);
        Map<String, Boolean> response = new HashMap<>();
        response.put("Deleted Region : " + region.getName() + " with ID : " + region.getId(), Boolean.TRUE);
        return ResponseEntity.ok(response);
    }

}

