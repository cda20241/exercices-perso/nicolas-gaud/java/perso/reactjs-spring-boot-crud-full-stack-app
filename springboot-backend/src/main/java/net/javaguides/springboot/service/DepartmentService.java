package net.javaguides.springboot.service;

import net.javaguides.springboot.exception.ResourceNotFoundException;
import net.javaguides.springboot.model.Department;
import net.javaguides.springboot.model.MarkerMotif;
import net.javaguides.springboot.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class DepartmentService {

    @Autowired
    private DepartmentRepository departmentRepository;

    public DepartmentService(DepartmentRepository departmentRepository) {
        this.departmentRepository = departmentRepository;
    }

    public Department getDepartmentById(String id){
        return departmentRepository.findDptById(id)
                .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundException.MESSAGE_DEPARTMENT_NOT_FOUND,
                        ResourceNotFoundException.LOG_DEPARTMENT_NOT_FOUND, -1));
    }

    /**
     * This function updates a Department in the DB, identified by its ID
     * @param id Integer
     * @param departmentDetails Department
     * @return ResponseEntity<Department>
     */
    public ResponseEntity<Department> updateDepartment(int id, Department departmentDetails) {
        Department department = departmentRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundException.MESSAGE_DEPARTMENT_NOT_FOUND, ResourceNotFoundException.LOG_DEPARTMENT_NOT_FOUND, id));

        department.setName(departmentDetails.getName());
        department.setRegion(departmentDetails.getRegion());

        Department updatedDepartment = departmentRepository.save(department);
        return ResponseEntity.ok(updatedDepartment);
    }

    /**
     * This function deletes a Department identified by its ID
     * @param id Integer
     * @return ResponseEntity<Map<String, Boolean>> Message indicating the Department has been deleted
     */
    public ResponseEntity<Map<String, Boolean>> deleteDepartment(int id){
        Department department = departmentRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundException.MESSAGE_DEPARTMENT_NOT_FOUND, ResourceNotFoundException.LOG_DEPARTMENT_NOT_FOUND, id));

        departmentRepository.delete(department);
        Map<String, Boolean> response = new HashMap<>();
        response.put("Deleted department : " + department.getName() + " with ID : " + department.getId(), Boolean.TRUE);
        return ResponseEntity.ok(response);
    }
}
