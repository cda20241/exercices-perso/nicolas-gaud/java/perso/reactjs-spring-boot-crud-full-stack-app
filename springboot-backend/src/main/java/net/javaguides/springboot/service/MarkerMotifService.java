package net.javaguides.springboot.service;

import net.javaguides.springboot.exception.ResourceNotFoundException;
import net.javaguides.springboot.model.Department;
import net.javaguides.springboot.model.MarkerMotif;
import net.javaguides.springboot.model.MarkerStatus;
import net.javaguides.springboot.repository.DepartmentRepository;
import net.javaguides.springboot.repository.MarkerMotifRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.HashMap;
import java.util.Map;

@Service
public class MarkerMotifService {

    @Autowired
    private MarkerMotifRepository markerMotifRepository;

    public MarkerMotifService(MarkerMotifRepository markerMotifRepository) {
        this.markerMotifRepository = markerMotifRepository;
    }

    public MarkerMotif getMarkerMotifById(int id){
        return markerMotifRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundException.MESSAGE_MARKER_MOTIF_NOT_FOUND,
                        ResourceNotFoundException.LOG_MARKER_STATUS_NOT_FOUND, id));
    }

    /**
     * This function updates a MarkerMotif in the DB, identified by its ID
     * @param id Integer
     * @param markerMotifDetails MarkerMotif
     * @return ResponseEntity<MarkerMotif>
     */
    public ResponseEntity<MarkerMotif> updateMarkerMotif(int id, MarkerMotif markerMotifDetails){
        MarkerMotif markerMotif = markerMotifRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundException.MESSAGE_MARKER_MOTIF_NOT_FOUND, ResourceNotFoundException.LOG_MARKER_MOTIF_NOT_FOUND, id));

        markerMotif.setLabel(markerMotifDetails.getLabel());

        MarkerMotif updatedMarkerMotif = markerMotifRepository.save(markerMotif);
        return ResponseEntity.ok(updatedMarkerMotif);
    }

    /**
     * This function deletes a MarkerMotif identified by its ID
     * @param id Integer
     * @return ResponseEntity<Map<String, Boolean>> Message indicating the MarkerMotif has been deleted
     */
    public ResponseEntity<Map<String, Boolean>> deleteMarkerMotif(int id) {
        MarkerMotif markerMotif = markerMotifRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundException.MESSAGE_MARKER_MOTIF_NOT_FOUND, ResourceNotFoundException.LOG_MARKER_MOTIF_NOT_FOUND, id));

        markerMotifRepository.delete(markerMotif);
        Map<String, Boolean> response = new HashMap<>();
        response.put("Deleted MarkerMotif : " + markerMotif.getLabel() + " with ID : " + markerMotif.getId(), Boolean.TRUE);
        return ResponseEntity.ok(response);
    }
}
