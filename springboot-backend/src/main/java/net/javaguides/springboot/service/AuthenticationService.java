package net.javaguides.springboot.service;

import net.javaguides.springboot.modeldto.UserAuthenticationDto;
import net.javaguides.springboot.auth.AuthenticationResponse;
import net.javaguides.springboot.constant.AppConstant;
import net.javaguides.springboot.model.User;
import net.javaguides.springboot.model.UserType;
import net.javaguides.springboot.repository.UserRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import static org.springframework.security.core.userdetails.User.builder;

@Service
public class AuthenticationService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;

    public AuthenticationService(UserRepository userRepository, PasswordEncoder passwordEncoder,
                                 JwtService jwtService, AuthenticationManager authenticationManager) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.jwtService = jwtService;
        this.authenticationManager = authenticationManager;
    }

    /**
     * This function registers a new User
     * @param userAuthenticationDto UserAuthenticationDto
     * @return ResponseEntity<AuthenticationResponse>
     */
    public ResponseEntity<AuthenticationResponse> register(UserAuthenticationDto userAuthenticationDto) {
        var user = builder()
                // Getting User's email
                .username(userAuthenticationDto.getEmail())
                // Getting and encoding User's password before saving it in DB
                .password(passwordEncoder.encode(userAuthenticationDto.getPassword()))
                // Setting 'User' role to the new User
                .roles(AppConstant.USER)
                .build();
        // Saving User in DB
        User userToInsert = new User(user.getUsername().substring(user.getUsername().lastIndexOf("@")),
                user.getUsername(), user.getPassword(),  new UserType(4, null, null));
        userRepository.save(userToInsert);
        // Generating User's token
        var jwtToken = jwtService.generateToken(user);
        return ResponseEntity.ok(AuthenticationResponse.builder()
                // Passing the token to the response sent to the client
                .token(jwtToken)
                .build());
    }

    /**
     * This function authenticates a new User
     * @param userAuthenticationDto UserAuthenticationDto
     * @return ResponseEntity<AuthenticationResponse>
     */
//    public ResponseEntity<AuthenticationResponse> authenticate(UserAuthenticationDto userAuthenticationDto) {
//        authenticationManager.authenticate(
//                // Generating authentication token of type username and password
//                new UsernamePasswordAuthenticationToken(
//                        userAuthenticationDto.getEmail(),
//                        userAuthenticationDto.getPassword()
//                )
//        );
//        // If email and password are correct, getting the User from DB
//        var user = userRepository.findUserByEmail(userAuthenticationDto.getEmail())
//                .orElseThrow();
//        // Generating User's token
//        var jwtToken = jwtService.generateToken(user);
//        return ResponseEntity.ok(AuthenticationResponse.builder()
//                // Passing the token to the response sent to the client
//                .token(jwtToken)
//                .build());
//    }

    public UserAuthenticationDto authenticate(UserAuthenticationDto userAuthenticationDto) {
        authenticationManager.authenticate(
                // Generating authentication token of type username and password
                new UsernamePasswordAuthenticationToken(
                        userAuthenticationDto.getEmail(),
                        userAuthenticationDto.getPassword()
                )
        );
        // If email and password are correct, getting the User from DB
        var user = userRepository.findUserByEmail(userAuthenticationDto.getEmail())
                .orElseThrow();
        // Generating User's token
        var jwtToken = jwtService.generateToken(user);
        userAuthenticationDto.setId(user.getId());
        userAuthenticationDto.setNickName(user.getNickname());
        userAuthenticationDto.setUserType(user.getUserType().getLabel());
        userAuthenticationDto.setUserTypeId(user.getUserType().getId());
        userAuthenticationDto.setToken(jwtToken);
        return userAuthenticationDto;
    }
}
