package net.javaguides.springboot.service;

import net.javaguides.springboot.exception.ResourceNotFoundException;
import net.javaguides.springboot.model.MarkerStatus;
import net.javaguides.springboot.model.MarkerType;
import net.javaguides.springboot.repository.MarkerStatusRepository;
import net.javaguides.springboot.repository.MarkerTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.HashMap;
import java.util.Map;

@Service
public class MarkerTypeService {

    @Autowired
    private MarkerTypeRepository markerTypeRepository;

    public MarkerTypeService(MarkerTypeRepository markerTypeRepository) {
        this.markerTypeRepository = markerTypeRepository;
    }

    public MarkerType getMarkerTypeById(int id){
        return markerTypeRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundException.MESSAGE_MARKER_TYPE_NOT_FOUND,
                        ResourceNotFoundException.LOG_MARKER_TYPE_NOT_FOUND, id));
    }

    /**
     * This function updates a MarkerType in the DB, identified by its ID
     * @param id Integer
     * @param markerTypeDetails MarkerType
     * @return ResponseEntity<MarkerType>
     */
    public ResponseEntity<MarkerType> updateMarkerType(int id, MarkerType markerTypeDetails){
        MarkerType markerType = markerTypeRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundException.MESSAGE_MARKER_TYPE_NOT_FOUND,
                        ResourceNotFoundException.LOG_MARKER_TYPE_NOT_FOUND, id));

        markerType.setLabel(markerTypeDetails.getLabel());

        MarkerType updatedMarkerType = markerTypeRepository.save(markerType);
        return ResponseEntity.ok(updatedMarkerType);
    }

    /**
     * This function deletes a MarkerType identified by its ID
     * @param id Integer
     * @return ResponseEntity<Map<String, Boolean>> Message indicating the MarkerType has been deleted
     */
    public ResponseEntity<Map<String, Boolean>> deleteMarkerType(int id){
        MarkerType markerType = markerTypeRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundException.MESSAGE_MARKER_TYPE_NOT_FOUND, ResourceNotFoundException.LOG_MARKER_TYPE_NOT_FOUND, id));

        markerTypeRepository.delete(markerType);
        Map<String, Boolean> response = new HashMap<>();
        response.put("Deleted MarkerType : " + markerType.getLabel() + " with ID : " + markerType.getId(), Boolean.TRUE);
        return ResponseEntity.ok(response);
    }
}
