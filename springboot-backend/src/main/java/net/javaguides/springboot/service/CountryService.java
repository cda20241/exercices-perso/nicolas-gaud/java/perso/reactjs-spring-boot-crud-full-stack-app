package net.javaguides.springboot.service;

import net.javaguides.springboot.exception.ResourceNotFoundException;
import net.javaguides.springboot.model.Country;
import net.javaguides.springboot.model.Region;
import net.javaguides.springboot.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class CountryService {

    @Autowired
    private final CountryRepository countryRepository;

    public CountryService(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    public Country getCountryById(int id){
        return countryRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundException.MESSAGE_COUNTRY_NOT_FOUND,
                        ResourceNotFoundException.LOG_COUNTRY_NOT_FOUND, id));
    }

    public ResponseEntity<Country> updateCountry(int id, Country countryDetails){
        Country country = countryRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundException.MESSAGE_COUNTRY_NOT_FOUND, ResourceNotFoundException.LOG_COUNTRY_NOT_FOUND, id));

        country.setFrName(countryDetails.getFrName());
        country.setEnName(countryDetails.getEnName());
        country.setFrCapitalCity(countryDetails.getFrCapitalCity());
        country.setEnCapitalCity(countryDetails.getEnCapitalCity());
        country.setContinentId(countryDetails.getContinentId());

        Country updatedCountry = countryRepository.save(country);
        return ResponseEntity.ok(updatedCountry);
    }

    public ResponseEntity<Map<String, Boolean>> deleteCountry(int id){
        Country country = countryRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundException.MESSAGE_COUNTRY_NOT_FOUND, ResourceNotFoundException.LOG_COUNTRY_NOT_FOUND, id));

        countryRepository.delete(country);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return ResponseEntity.ok(response);
    }

}

