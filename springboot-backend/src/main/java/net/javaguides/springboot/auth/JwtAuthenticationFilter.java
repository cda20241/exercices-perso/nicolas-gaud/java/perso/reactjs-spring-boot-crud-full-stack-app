package net.javaguides.springboot.auth;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.NonNull;
import net.javaguides.springboot.service.JwtService;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private final JwtService jwtService;

    // Instantiating 'UserDetailsService' from Spring
    private final UserDetailsService userDetailsService;

    public JwtAuthenticationFilter(JwtService jwtService, UserDetailsService userDetailsService) {
        this.jwtService = jwtService;
        this.userDetailsService = userDetailsService;
    }

    @Override
    protected void doFilterInternal(

            @NonNull HttpServletRequest request,
            @NonNull HttpServletResponse response,
            @NonNull FilterChain filterChain

    )
            throws ServletException, IOException {

        // Implementing 'authHeader' to which we have to pass the authentication token JWT
        // each time we call the API
        final String authHeader = request.getHeader("Authorization");

        // variable that will contain the token
        final String jwt;

        // extraction of username (login) which is for us the user's eamil
        final String userEmail;

        // verifying if variables are not empty or not valid
        if (authHeader == null || !authHeader.startsWith("Bearer ")) {
            filterChain.doFilter(request, response);
            return;
        }

        // Getting the JWT token from 'authHeader'
        jwt = authHeader.substring(7);
        userEmail = jwtService.extractUsername(jwt);

        // If the User is already connected, no need to verify again his token validity
        if(userEmail != null && SecurityContextHolder.getContext().getAuthentication() == null) {
            UserDetails userDetails = this.userDetailsService.loadUserByUsername(userEmail);
            if(jwtService.isTokenValid(jwt, userDetails)) {
                // Creating authentication token for the User
                UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
                        userDetails,
                        null,
                        userDetails.getAuthorities()
                );
                // Adding details from the request to the token
                authToken.setDetails(
                        new WebAuthenticationDetailsSource().buildDetails(request)
                );
                // Updating the SecurityContextHolder with the 'authToken' generated above
                SecurityContextHolder.getContext().setAuthentication(authToken);
            }
        }
        // Then we leave the hand to the next filter to be executed
        filterChain.doFilter(request, response);
    }
}