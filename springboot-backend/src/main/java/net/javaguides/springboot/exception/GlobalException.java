package net.javaguides.springboot.exception;

import java.io.Serial;

public class GlobalException extends RuntimeException{

    @Serial
    private static final long serialVersionUID = 1L;

    private String message = "";

    public GlobalException(String message) {
        super(message);
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
