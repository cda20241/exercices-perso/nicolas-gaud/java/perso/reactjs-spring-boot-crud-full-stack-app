package net.javaguides.springboot.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.Serial;
import java.io.Serializable;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends GlobalException implements Serializable {

	@Serial
	private static final long serialVersionUID = 1L;

	public static final String MESSAGE_RESOURCE_NOT_FOUND = "Error : Resource not found.";

	public static final String MESSAGE_COUNTRY_NOT_FOUND = "Error : Country not found.";

	public static final String LOG_COUNTRY_NOT_FOUND = "No country found in DB with Id : ";

	public static final String MESSAGE_DEPARTMENT_NOT_FOUND = "Error : Department not found.";

	public static final String LOG_DEPARTMENT_NOT_FOUND = "No department found in DB with Id : ";

	public static final String MESSAGE_MARKER_NOT_FOUND = "Error : Marker not found.";

	public static final String LOG_MARKER_NOT_FOUND = "No marker found in DB with Id : ";

	public static final String MESSAGE_REGION_NOT_FOUND = "Error : Region not found.";

	public static final String LOG_REGION_NOT_FOUND = "No region found in DB with Id : ";

	public static final String MESSAGE_USER_NOT_FOUND = "Error : User not found.";

	public static final String LOG_USER_NOT_FOUND = "No user found in DB with Id : ";

	public static final String MESSAGE_USER_MARKER_NOT_FOUND = "Error : UserMarker not found.";

	public static final String LOG_USER_MARKER_NOT_FOUND = "No userMarker found in DB with Id : ";

	public static final String MESSAGE_MARKER_MOTIF_NOT_FOUND = "Error : Motif not found.";

	public static final String LOG_MARKER_MOTIF_NOT_FOUND = "No motif found in DB with Id : ";

	public static final String MESSAGE_MARKER_STATUS_NOT_FOUND = "Error : Status not found.";

	public static final String LOG_MARKER_STATUS_NOT_FOUND = "No status found in DB with Id : ";

	public static final String MESSAGE_MARKER_TYPE_NOT_FOUND = "Error : Type not found.";

	public static final String LOG_MARKER_TYPE_NOT_FOUND = "No type found in DB with Id : ";

	private String log = "";
	private int id;

	public ResourceNotFoundException() {
		super(MESSAGE_RESOURCE_NOT_FOUND);
	}

	public ResourceNotFoundException(String message, String log, int id) {
		super(message);
		this.log = log;
		this.id = id;
	}

	public String getLog() {
		return log + id;
	}

	public void setLog(String log, Long id) {
		this.log = log + id;
	}
}
