package net.javaguides.springboot.controller;

import net.javaguides.springboot.constant.AppConstant;
import net.javaguides.springboot.exception.ResourceNotFoundException;
import net.javaguides.springboot.model.Marker;
import net.javaguides.springboot.model.User;
import net.javaguides.springboot.model.UserMarker;
import net.javaguides.springboot.repository.UserMarkerRepository;
import net.javaguides.springboot.responsedto.UserMarkerResponseDTO;
import net.javaguides.springboot.service.UserMarkerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@CrossOrigin(origins = AppConstant.CROSS_ORIGIN)
@RestController
@RequestMapping("/userMarkers")
public class UserMarkerController {

    @Autowired
    private UserMarkerService userMarkerService;

    public UserMarkerController(UserMarkerService userMarkerService) {
        this.userMarkerService = userMarkerService;
    }

    /**
     * Simply gets all UserMarkers
     * @return List<UserMarker>
     */
    @GetMapping("/all")
    public List<UserMarker> getAllUserMarker(){
        return userMarkerService.getAllUserMarker();
    }


    /**
     * This function inserts a UserMarker in the DB
     * @param marker Marker
     * @param user User
     * @return ResponseEntity<UserMarkerResponseDTO>
     */
    @PostMapping("")
    public ResponseEntity<UserMarkerResponseDTO> insertUserMarker(@RequestBody Marker marker, @RequestBody User user) {
        return userMarkerService.insertUserMarker(marker, user);
    }

    /**
     * This function deletes a UserMarker from DB defined by iys Id
     * @param id int
     * @return ResponseEntity<UserMarkerResponseDTO>
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<UserMarkerResponseDTO> deleteUserMarker(@PathVariable int id){
        return userMarkerService.deleteUserMarker(id);
    }

    /**
     * Gets a UserMarker defined by its Id
     * @param id int
     * @return UserMarker
     */
    @GetMapping("/{id}")
    public UserMarker getUserMarkerById(@PathVariable int id) {
        return userMarkerService.getUserMarkerById(id);
    }

    /**
     * Gets the number of Users went to a Marker defined by its Id
     * @param id int
     * @return ResponseEntity<UserMarkerResponseDTO>
     */
    @GetMapping("/count/{id}")
    public Integer getCountUserMarkersByMarkerId(@PathVariable int id){
        return userMarkerService.getCountUserMarkersByMarkerId(id);
    }


}
