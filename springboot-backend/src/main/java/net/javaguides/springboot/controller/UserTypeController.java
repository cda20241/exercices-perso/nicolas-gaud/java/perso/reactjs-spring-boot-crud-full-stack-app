package net.javaguides.springboot.controller;


import net.javaguides.springboot.constant.AppConstant;
import net.javaguides.springboot.model.UserType;
import net.javaguides.springboot.repository.UserTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = AppConstant.CROSS_ORIGIN)
@RestController
@RequestMapping("/userTypes")
public class UserTypeController {

    @Autowired
    private UserTypeRepository userTypeRepository;

    // get all users
    @GetMapping("")
    public List<UserType> getAllUserTypes(){
        return userTypeRepository.findAll();
    }
}