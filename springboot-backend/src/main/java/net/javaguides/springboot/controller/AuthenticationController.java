package net.javaguides.springboot.controller;

import net.javaguides.springboot.modeldto.UserAuthenticationDto;
import net.javaguides.springboot.auth.AuthenticationResponse;
import net.javaguides.springboot.constant.AppConstant;
import net.javaguides.springboot.service.AuthenticationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = AppConstant.CROSS_ORIGIN)
@RestController
@RequestMapping("/auth")
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    public AuthenticationController(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @PostMapping("/register")
    public ResponseEntity<AuthenticationResponse> register(
            @RequestBody UserAuthenticationDto userAuthenticationDto
    ) {
        return authenticationService.register(userAuthenticationDto);
    }

    @PostMapping("/authenticate")
    public UserAuthenticationDto authenticate (
            @RequestBody UserAuthenticationDto userAuthenticationDto
    ) {
        return authenticationService.authenticate(userAuthenticationDto);
    }
}
