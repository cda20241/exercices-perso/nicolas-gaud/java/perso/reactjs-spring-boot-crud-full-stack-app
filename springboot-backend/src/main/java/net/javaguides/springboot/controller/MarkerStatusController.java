package net.javaguides.springboot.controller;

import net.javaguides.springboot.constant.AppConstant;
import net.javaguides.springboot.exception.ResourceNotFoundException;
import net.javaguides.springboot.model.MarkerStatus;
import net.javaguides.springboot.repository.MarkerStatusRepository;
import net.javaguides.springboot.service.MarkerStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@CrossOrigin(origins = AppConstant.CROSS_ORIGIN)
@RestController
@RequestMapping("/markerStatuses")
public class MarkerStatusController {
    @Autowired
    private MarkerStatusRepository markerStatusRepository;

    @Autowired
    private MarkerStatusService markerStatusService;

    public MarkerStatusController(MarkerStatusRepository markerStatusRepository, MarkerStatusService markerStatusService) {
        this.markerStatusRepository = markerStatusRepository;
        this.markerStatusService = markerStatusService;
    }

    // get all markerStatuses
    @GetMapping("")
    public List<MarkerStatus> getAllMarkerStatuses(){
        return markerStatusRepository.findAll();
    }


    // create markerStatus rest api
    @PostMapping("")
    public MarkerStatus createMarkerStatus(@RequestBody MarkerStatus markerStatus) {
        return markerStatusRepository.save(markerStatus);
    }

    // get markerStatus by id rest api
    @GetMapping("/{id}")
    public MarkerStatus getMarkerStatusById(@PathVariable int id) {
        return markerStatusRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundException.MESSAGE_MARKER_STATUS_NOT_FOUND, ResourceNotFoundException.LOG_MARKER_STATUS_NOT_FOUND, id));
    }

    // update markerStatus rest api
    @PutMapping("/{id}")
    public ResponseEntity<MarkerStatus> updateMarkerStatus(@PathVariable int id, @RequestBody MarkerStatus markerStatusDetails){
        return markerStatusService.updateMarkerStatus(id, markerStatusDetails);
    }

    // delete markerStatus rest api
    @DeleteMapping("/{id}")
    public ResponseEntity<Map<String, Boolean>> deleteMarkerStatus(@PathVariable int id){
        return markerStatusService.deleteMarkerStatus(id);
    }
}
