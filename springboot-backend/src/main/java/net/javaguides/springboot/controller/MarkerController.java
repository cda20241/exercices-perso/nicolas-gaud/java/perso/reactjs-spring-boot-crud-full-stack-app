package net.javaguides.springboot.controller;

import net.javaguides.springboot.constant.AppConstant;
import net.javaguides.springboot.model.Marker;
import net.javaguides.springboot.model.User;
import net.javaguides.springboot.modeldto.MarkerCountIsUrbexDTO;
import net.javaguides.springboot.modeldto.MarkerCountUsersDTO;
import net.javaguides.springboot.modeldto.MarkerCreationDTO;
import net.javaguides.springboot.modeldto.MarkerDTO;
import net.javaguides.springboot.repository.MarkerRepository;
import net.javaguides.springboot.responsedto.MarkerResponseDTO;
import net.javaguides.springboot.service.MarkerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@CrossOrigin(origins = AppConstant.CROSS_ORIGIN)
@RestController
@RequestMapping("/markers")
public class MarkerController {

    // TODO Validate on update / create

    @Autowired
    private final MarkerRepository markerRepository;

    @Autowired
    private final MarkerService markerService;

    // Constructors
    public MarkerController(MarkerRepository markerRepository, MarkerService markerService) {
        this.markerRepository = markerRepository;
        this.markerService = markerService;
    }

    /**
     * Gets all Markers, depending on marker.levelId <= userTypeId
     * @param userTypeId int
     * @return List<MarkerDTO>
     */
    @GetMapping("/all/{userTypeId}")
    public ResponseEntity<Map<String, Object>> getMarkersPerPage(
            @PathVariable int userTypeId,
            @RequestParam(defaultValue = AppConstant.DEFAULT_INITIAL_PAGE) int page,
            @RequestParam(defaultValue = AppConstant.DEFAULT_MARKER_PER_PAGE) int size,
            @RequestParam(defaultValue = AppConstant.SORT_ASC) String sort) {
        Page<MarkerDTO> pageData = markerService.findMarkersPerPage(userTypeId, page, size, sort);
        int totalItems = markerService.countMarkers(userTypeId);

        Map<String, Object> response = new HashMap<>();
        response.put("markers", pageData.getContent());
        response.put("totalItems", totalItems);
        response.put("totalPages", pageData.getTotalPages());
        response.put("currentPage", pageData.getNumber());

        return ResponseEntity.ok().body(response);
    }

    /**
     * Get Markers count found in DB within a Set of ids
     * @param ids Set<Integer>
     * @return Integer
     */
    @GetMapping("/countByIds")
    public Integer countMarkers(@RequestBody Set<Integer> ids){
        return markerService.countMarkers(ids);
    }


    /**
     * Get a list of Markers limited by default at 32 records / page
     * Result records must be inside a given Set of Ids
     * @See Pageable
     */
    @GetMapping("/pagedDataPerIds")
    public List<MarkerDTO> findMarkersPerPage(
            @RequestParam(defaultValue = AppConstant.DEFAULT_INITIAL_PAGE) int page,
            @RequestParam(defaultValue = AppConstant.DEFAULT_MARKER_PER_PAGE) int size,
            @RequestParam(defaultValue = AppConstant.SORT_ASC) String sort,
            @RequestBody Set<Integer> ids) {
        return markerService.findMarkersPerPage(page, size, sort, ids);
    }

    /**
     * Gets a MarkerDTO defined by its ID and levelId, within a Set of given IDs
     * @param id int
     * @param userTypeId int
     * @param dir String ("asc" or "desc")
     * @param ids Set<Integer>
     * @return Optional<MarkerDTO>
     */
    @GetMapping("/{id}/{userTypeId}/{dir}")
    public ResponseEntity<Optional<MarkerDTO>> findMarkerById(
            @PathVariable int id,
            @PathVariable int userTypeId,
            @PathVariable String dir,
            @RequestBody Set<Integer> ids){
            Optional<MarkerDTO> markerDTO = markerService.findMarkerById(id, userTypeId, dir, ids);
            if (markerDTO.isPresent())
                return ResponseEntity.ok(markerDTO);
            else
                return null;
    }

    /**
     * Gets a MarkerDTO defined by its ID within a Set of give, IDs
     * @param id int
     * @return Optional<MarkerDTO>
     */
    @GetMapping("/{id}")
    public ResponseEntity<Optional<MarkerDTO>> findMarkerById(
            @PathVariable int id){
        Optional<MarkerDTO> markerDTO = markerService.findMarkerById(id);
        if (markerDTO.isPresent())
            return ResponseEntity.ok(markerDTO);
        else
            return null;
    }

    /**
     * Gets all Marker names (used to autocomplete search text input in Front End
     * @param userTypeId int
     * @return List<String>
     */
    @GetMapping("/markerNames")
    public List<String> findAllMarkerNames(@RequestParam int userTypeId){
        return markerService.findAllMarkerNames(userTypeId);
    }

    /**
     * Gets how many Marker are 'Urbex' and how many are not
     * @param userTypeId int
     * @return List<MarkerCountIsUrbexDTO>
     */
    @GetMapping("/countIsUrbex")
    public List<MarkerCountIsUrbexDTO> countIsUrbex(@RequestParam("userTypeId") int userTypeId){
        return markerRepository.countIsUrbex(userTypeId);
    }

    /**
     * Gets how many Marker are 'Urbex' and how many are not, triggered when user chooses filters
     * and refreshes the counts
     * @param ids Set<Integer>
     * @param userTypeId int
     * @return List<MarkerCountIsUrbexDTO>
     */
    @GetMapping("/updateIsUrbex")
    public List<MarkerCountIsUrbexDTO> updateIsUrbex(@RequestBody Set<Integer> ids, @RequestParam int userTypeId){
        return markerService.updateIsUrbex(ids, userTypeId);
    }

    /**
     * Gets how many Marker have been visited by each User
     * @param userTypeId int
     * @return List<MarkerCountUsersDTO>
     */
    @GetMapping("/countMarkersPerUser")
    public List<MarkerCountUsersDTO> countIsVisited(@RequestParam("userTypeId") int userTypeId){
        return markerService.countIsVisited(userTypeId);
    }


    /**
     * Gets how many Marker have been visited by each User, triggered when user chooses filters
     * and refreshes the counts
     * @param userTypeId int
     * @param ids Set<Integer>
     * @return List<MarkerCountUsersDTO>
     */
    @GetMapping("/updateMarkersPerUser")
    public List<MarkerCountUsersDTO> updateIsVisited(@RequestBody Set<Integer> ids, @RequestParam int userTypeId){
        return markerService.updateIsVisited(ids, userTypeId);
    }

    /**
     * Gets the min ID from DB contained in a Set of Ids
     * @param ids Set<Integer>
     * @return Integer
     */
    @GetMapping("/minId")
    public Integer getMinId(@RequestBody Set<Integer> ids){
        return  markerService.getMinId(ids);
    }

    /**
     * Gets the max ID from DB contained in a Set of Ids
     * @param ids Set<Integer>
     * @return Integer
     */
    @GetMapping("/maxId")
    public Integer getMaxId(@RequestBody Set<Integer> ids){
        return markerService.getMaxId(ids);
    }

    /**
     * This function inserts a Marker in the DB
     * @param markerCreationDTO MarkerCreationDTO // to be mapped
     * @param user User // to set marker.levelId = user.userTypeId
     * @return Integer
     */
    @PostMapping("")
    public Integer insertMarker(@RequestBody MarkerCreationDTO markerCreationDTO, @RequestBody User user) {
        return markerService.insertMarker(markerCreationDTO, user);
    }

    /**
     * This function updates a Marker in the DB with new values depending on its attributes
     * @param markerDetails Marker
     * @return ResponseEntity<Marker>
     */
    @PutMapping("")
    public ResponseEntity<MarkerResponseDTO> updateMarker(@RequestBody MarkerDTO markerDetails){
        return markerService.updateMarker(markerDetails);
    }

    /**
     * This function deletes a Marker identified by its ID, deleting all associated images in cascade @See Marker
     * @param id int
     * @return ResponseEntity<DeletedMarkerResponseDTO>
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<MarkerResponseDTO> deleteMarker(@PathVariable int id){
        return markerService.deleteMarkerAndImages(id);
    }



}
