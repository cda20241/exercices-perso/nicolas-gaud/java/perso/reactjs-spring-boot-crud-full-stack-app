package net.javaguides.springboot.controller;

import net.javaguides.springboot.constant.AppConstant;
import net.javaguides.springboot.exception.ResourceNotFoundException;
import net.javaguides.springboot.model.Region;
import net.javaguides.springboot.repository.RegionRepository;
import net.javaguides.springboot.service.RegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@CrossOrigin(origins = AppConstant.CROSS_ORIGIN)
@RestController
@RequestMapping("/regions")
public class RegionController {
    @Autowired
    private final RegionRepository regionRepository;

    @Autowired
    private final RegionService regionService;

    public RegionController(RegionRepository regionRepository, RegionService regionService) {
        this.regionRepository = regionRepository;
        this.regionService = regionService;
    }

    // get all regions
    @GetMapping("")
    public List<Region> getAllRegions(){
        return regionRepository.findAll();
    }


    // create region rest api
    @PostMapping("")
    public Region createRegion(@RequestBody Region region) {
        return regionRepository.save(region);
    }

    // get region by id rest api
    @GetMapping("/{id}")
    public Region getRegionById(@PathVariable int id) {
        return regionRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundException.MESSAGE_REGION_NOT_FOUND, ResourceNotFoundException.LOG_REGION_NOT_FOUND, id));
    }

    // update region rest api
    @PutMapping("/{id}")
    public ResponseEntity<Region> updateRegion(@PathVariable int id, @RequestBody Region regionDetails){
        return regionService.updateRegion(id, regionDetails);
    }

    // delete region rest api
    @DeleteMapping("/{id}")
    public ResponseEntity<Map<String, Boolean>> deleteRegion(@PathVariable int id){
        return regionService.deleteRegion(id);
    }
}
