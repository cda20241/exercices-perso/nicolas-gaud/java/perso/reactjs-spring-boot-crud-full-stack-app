package net.javaguides.springboot.controller;

import net.javaguides.springboot.constant.AppConstant;
import net.javaguides.springboot.exception.ResourceNotFoundException;
import net.javaguides.springboot.model.Department;
import net.javaguides.springboot.repository.DepartmentRepository;
import net.javaguides.springboot.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = AppConstant.CROSS_ORIGIN)
@RestController
@RequestMapping("/departments")
public class DepartmentController {
    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private DepartmentService departmentService;

    public DepartmentController(DepartmentRepository departmentRepository, DepartmentService departmentService) {
        this.departmentRepository = departmentRepository;
        this.departmentService = departmentService;
    }

    // get all departments
    @GetMapping("")
    public List<Department> getAllDepartments(){
        return departmentRepository.findAll();
    }


    // create department rest api
    @PostMapping("")
    public Department createDepartment(@RequestBody Department department) {
        return departmentRepository.save(department);
    }

    // get department by id rest api
    @GetMapping("/{id}")
    public Department getDepartmentById(@PathVariable int id) {
        return departmentRepository.findDptById(String.valueOf(id))
                .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundException.MESSAGE_DEPARTMENT_NOT_FOUND, ResourceNotFoundException.LOG_DEPARTMENT_NOT_FOUND, id));
    }

    // update department rest api
    @PutMapping("/{id}")
    public ResponseEntity<Department> updateDepartment(@PathVariable int id, @RequestBody Department departmentDetails){
        return departmentService.updateDepartment(id, departmentDetails);
    }

    // delete department rest api
    @DeleteMapping("/{id}")
    public ResponseEntity<Map<String, Boolean>> deleteDepartment(@PathVariable int id){
        return departmentService.deleteDepartment(id);
    }
}
