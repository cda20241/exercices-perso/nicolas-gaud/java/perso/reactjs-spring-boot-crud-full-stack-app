package net.javaguides.springboot.controller;

import net.javaguides.springboot.constant.AppConstant;
import net.javaguides.springboot.exception.ResourceNotFoundException;
import net.javaguides.springboot.model.Country;
import net.javaguides.springboot.repository.CountryRepository;
import net.javaguides.springboot.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@CrossOrigin(origins = AppConstant.CROSS_ORIGIN)
@RestController
@RequestMapping("/countries")
public class CountryController {
    @Autowired
    private final CountryRepository countryRepository;

    @Autowired
    private final CountryService countryService;

    public CountryController(CountryRepository countryRepository, CountryService countryService) {
        this.countryRepository = countryRepository;
        this.countryService = countryService;
    }

    // get all countries
    @GetMapping("")
    public List<Country> getAllCountries(){
        return countryRepository.findAll();
    }


    // create country rest api
    @PostMapping("")
    public Country createCountry(@RequestBody Country country) {
        return countryRepository.save(country);
    }

    // get country by id rest api
    @GetMapping("/{id}")
    public Country getCountryById(@PathVariable int id) {
        return countryRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundException.MESSAGE_COUNTRY_NOT_FOUND, ResourceNotFoundException.LOG_COUNTRY_NOT_FOUND, id));
    }

    // update country rest api
    @PutMapping("/{id}")
    public ResponseEntity<Country> updateCountry(@PathVariable int id, @RequestBody Country countryDetails){
       return countryService.updateCountry(id, countryDetails);
    }

    // delete country rest api
    @DeleteMapping("/{id}")
    public ResponseEntity<Map<String, Boolean>> deleteCountry(@PathVariable int id){
        return countryService.deleteCountry(id);
    }
}
