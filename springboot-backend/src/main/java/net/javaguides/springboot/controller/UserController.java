package net.javaguides.springboot.controller;

import net.javaguides.springboot.constant.AppConstant;
import net.javaguides.springboot.model.User;
import net.javaguides.springboot.responsedto.UserResponseDTO;
import net.javaguides.springboot.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = AppConstant.CROSS_ORIGIN)
@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    /**
     * This function gets all Users
     * @return List<User>
     */
    @GetMapping("/all")
    public List<User> getAllUsers(){
        return userService.getAllUsers();
    }

    /**
     * This function retrieves a User defined by its Id
     * @param id int
     * @return User
     */
    @GetMapping("")
    public User getUserById(@RequestParam int id) {
        return userService.getUserById(id);
    }

    /**
     * This function retrieves a User by its email
     * @param email String
     * @return User
     */
    @GetMapping("/{email}")
    public User getUserByEmail(@PathVariable String email) {
        return userService.getUserByEmail(email);
    }

    /**
     * This function saves a User in the DB
     * @param user User
     * @return User
     */
    @PostMapping("")
    public User createUser(@RequestBody User user) {
        return userService.createUser(user);
    }

    /**
     * This function updates a User by its given ID
     * @param id Integer
     * @param userDetails User containing data to be updated
     * @return ResponseEntity<User>
     */
    @PutMapping("/{id}")
    public ResponseEntity<UserResponseDTO> updateUser(@PathVariable int id, @RequestBody User userDetails){
        return userService.updateUser(id, userDetails);
    }

    /**
     * This function deletes a User from database by its given ID after having verified the User exists in DB
     * @param id Integer
     * @return ResponseEntity<Map<String, Boolean>>
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<UserResponseDTO> deleteUser(@PathVariable int id){
        return userService.deleteUser(id);
    }
}