package net.javaguides.springboot.controller;

import net.javaguides.springboot.constant.AppConstant;
import net.javaguides.springboot.exception.ResourceNotFoundException;
import net.javaguides.springboot.model.MarkerMotif;
import net.javaguides.springboot.repository.MarkerMotifRepository;
import net.javaguides.springboot.service.MarkerMotifService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = AppConstant.CROSS_ORIGIN)
@RestController
@RequestMapping("/markerMotifs")
public class MarkerMotifController {
    @Autowired
    private MarkerMotifRepository markerMotifRepository;

    @Autowired
    private MarkerMotifService markerMotifService;

    public MarkerMotifController(MarkerMotifRepository markerMotifRepository, MarkerMotifService markerMotifService) {
        this.markerMotifRepository = markerMotifRepository;
        this.markerMotifService = markerMotifService;
    }

    // get all markerMotifes
    @GetMapping("/all")
    public List<MarkerMotif> getAllMarkerMotifs(){
        return markerMotifRepository.findAll();
    }


    // create markerMotif rest api
    @PostMapping("")
    public MarkerMotif createMarkerMotif(@RequestBody MarkerMotif markerMotif) {
        return markerMotifRepository.save(markerMotif);
    }

    // get markerMotif by id rest api
    @GetMapping("")
    public MarkerMotif getMarkerMotifById(@RequestParam("id") int id) {
        return markerMotifRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundException.MESSAGE_MARKER_MOTIF_NOT_FOUND, ResourceNotFoundException.LOG_MARKER_MOTIF_NOT_FOUND, id));
    }

    // update markerMotif rest api
    @PutMapping("/{id}")
    public ResponseEntity<MarkerMotif> updateMarkerMotif(@PathVariable int id, @RequestBody MarkerMotif markerMotifDetails){
        return markerMotifService.updateMarkerMotif(id, markerMotifDetails);
    }

    // delete markerMotif rest api
    @DeleteMapping("/{id}")
    public ResponseEntity<Map<String, Boolean>> deleteMarkerMotif(@PathVariable int id){
        return markerMotifService.deleteMarkerMotif(id);
    }
}
