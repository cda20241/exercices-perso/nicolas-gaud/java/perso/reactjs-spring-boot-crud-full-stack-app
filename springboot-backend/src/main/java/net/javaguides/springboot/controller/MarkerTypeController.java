package net.javaguides.springboot.controller;

import net.javaguides.springboot.constant.AppConstant;
import net.javaguides.springboot.exception.ResourceNotFoundException;
import net.javaguides.springboot.model.MarkerType;
import net.javaguides.springboot.repository.MarkerTypeRepository;
import net.javaguides.springboot.service.MarkerTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@CrossOrigin(origins = AppConstant.CROSS_ORIGIN)
@RestController
@RequestMapping("/markerTypes")
public class MarkerTypeController {
    @Autowired
    private MarkerTypeRepository markerTypeRepository;

    @Autowired
    private MarkerTypeService markerTypeService;

    public MarkerTypeController(MarkerTypeRepository markerTypeRepository, MarkerTypeService markerTypeService) {
        this.markerTypeRepository = markerTypeRepository;
        this.markerTypeService = markerTypeService;
    }

    // get all markerTypes
    @GetMapping("")
    public List<MarkerType> getAllMarkerTypes(){
        return markerTypeRepository.findAll();
    }


    // create markerType rest api
    @PostMapping("")
    public MarkerType createMarkerType(@RequestBody MarkerType markerType) {
        return markerTypeRepository.save(markerType);
    }

    // get markerType by id rest api
    @GetMapping("/{id}")
    public MarkerType getMarkerTypeById(@PathVariable int id) {
        return markerTypeRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundException.MESSAGE_MARKER_TYPE_NOT_FOUND, ResourceNotFoundException.LOG_MARKER_TYPE_NOT_FOUND, id));
    }

    // update markerType rest api
    @PutMapping("/{id}")
    public ResponseEntity<MarkerType> updateMarkerType(@PathVariable int id, @RequestBody MarkerType markerTypeDetails){
        return markerTypeService.updateMarkerType(id, markerTypeDetails);
    }

    // delete markerType rest api
    @DeleteMapping("/{id}")
    public ResponseEntity<Map<String, Boolean>> deleteMarkerType(@PathVariable int id){
        return markerTypeService.deleteMarkerType(id);
    }
}
